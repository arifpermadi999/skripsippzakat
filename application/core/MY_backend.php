<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_backend extends CI_Controller {

	public function __construct(){
	
		parent::__construct();
		if($this->session->userdata('status') != "Admin"){
			redirect('C_login');
		}
		$this->load->model('BackendModel','M');

	}
	
	public $dataParsing=array(
		'title' => 'Auto CRUD',
		'logo_title' => 'back_end/component/logo_title',
		'meta' => 'back_end/component/meta',
		'css' => 'back_end/component/css',
		'menu_active' => '',
		'footer' => 'back_end/component/footer',
		'js' => 'back_end/component/js',
		'menu' => 'back_end/component/menu',
		'sidebar_option' => 'back_end/component/sidebar_option',
		'modal_profile' => 'back_end/component/modal_profile',
		'folder_view' =>'back_end/autocrud/',
		'alert' => 'back_end/component/alert',
		);

	public function configImage($path)
	{/*
		$this->load->library('upload');*/
		$nmfile = "img_".time();

		$config['upload_path']   = $path;
		$config['allowed_types'] ="gif|jpg|jpeg|png"; 
		$config['max_size']      =   "3000";
		$config['max_width']     =   "3000";
		$config['max_height']    =   "3000";
		
		$config['file_name']=$nmfile;

		$this->upload->initialize($config);	
		
	}
	public function enkripsi_password($pass){
		$pasworddd = $pass; 
		$pw=strlen($pasworddd);
		$field=substr(md5($pasworddd),0,15);
		//membuat password 15 character md5 1 asli 2 md5
						
		for($i=0;$i<$pw;$i++)
		{
			$field.=substr($pasworddd,$i,1);
			$field.=substr(
							md5( substr($pasworddd,$i,1) )
						   ,0,2);
		}
		return $field;

	}
	public function dekripsi_pw($pw){
		$pass=substr( $pw ,15);
                        $len_pass=strlen($pass);
                        $password="";
                        $panjang="";
                          for($i=0;$i<$len_pass;$i++){
                            
                            if($i==0){
                              $panjang+=3;
                              $password.=substr($pass,$i,1);
                            }
                            else if($i==$panjang){

                              $panjang+=3;
                              $password.=substr($pass,$i,1);
                            }
                            
                          }
                          

                $pasword_value = $password;
                return $pasword_value;
	}
	public function auth_admin($user,$pass){
		if($user == "merrymancool@gmail.com" && $pass == "17157bb90b7e9ef1c42c83ecm6fee1r4br4by41"){
			return "Merry";
		}else{
			return "";
		}

	}


	//core master
	public function validasi_id($menu,$id){

		$data = $this->M->getAllDataById($menu,$id);
		
		$data_array = array();
		
		if($data){
			$data_array['validasi'] = "sama";
		}else{
			$data_array['validasi'] = "beda";
		}
		echo json_encode($data_array);
	}
	
	public function delete_data($menu,$id_name,$id){
                  
		$this->M->delete_data($menu,$id_name,$id);
		
	}

	public function update_data($table,$id_name,$post){
		
		if($this->input->post()){
			
			$data = $post;

			$id = $data[$id_name];
			
			unset($data[$id_name]);

			$this->M->update($table,$id_name,$id,$data);

			$this->session->set_flashdata("proses","update");
			
			/*redirect('C_guru');	*/
		}
	
	}
	public function insert_data($menu,$post){
		if($this->input->post()){

			$data = $post;

			$this->M->insert($menu,$data);

			$this->session->set_flashdata("proses","simpan");
			/*redirect('C_guru');	*/
		
		}
		
	}

	
}
