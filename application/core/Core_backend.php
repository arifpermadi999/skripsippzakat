<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_backend extends CI_Controller {

	public $dataParsing = array(
		'menu_klik' => '' ,
		'meta' => 'back_end/component/meta',
		'css' => 'back_end/component/css',
		'menu' => 'back_end/component/menu',
		'js' => 'back_end/component/js',
		'footer' => 'back_end/component/footer',
		'sidebar_option' => 'back_end/component/sidebar_option', 
		'logo' => 'back_end/component/logo_title' 
		'alert' => 'back_end/component/alert'
		);

	
}
