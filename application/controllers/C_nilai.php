<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_nilai extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_nilai','M');
		$this->dataParsing['title'] = "Nilai";
		$this->dataParsing['menu_active'] = "nilai";
	}
	public function index()
	{
		$this->dataParsing['kelas'] = $this->M->getAllDataKelas();
		$this->dataParsing['siswa'] = $this->M->getAllDataSiswa();
		$this->load->view('back_end/list_nilai',$this->dataParsing);
	}
	function lihat_nilai_siswa(){
		$this->dataParsing['nis'] = $this->session->userdata('kode');
		
		$data_siswa = $this->db->where('nis',$this->session->userdata('kode'))->get('tbl_siswa')->row_object();
		
		$data_kelas = $this->db->where('id_kelas',$data_siswa->id_kelas)->get('tbl_kelas')->row_object();

		$this->dataParsing['id_kelas'] = $data_kelas->id_kelas;

		$this->dataParsing['kelas'] = $data_kelas->kelas;
		$this->dataParsing['nama'] = $data_siswa->nama;

		$this->dataParsing['data_kelas_lama'] = $this->db->query("select distinct n.id_kelas,k.kelas from tbl_nilai n,tbl_kelas k where n.id_kelas = k.id_kelas and n.nis = '".$this->session->userdata('kode')."'")->result_object();

		$this->dataParsing['data'] = $this->M->getAllDataNilai($data_kelas->id_kelas,$this->session->userdata('kode'));
		$this->dataParsing['data2'] = $this->M->getAllDataNilaiLama($this->session->userdata('kode'));
		
		$this->load->view('back_end/list_nilai_siswa',$this->dataParsing);	

	}
	function edit_nilai($id_kelas,$id_siswa){
		$this->dataParsing['id_kelas'] = $id_kelas;
		$this->dataParsing['nis'] = $id_siswa;
		
		$data = $this->db->where('id_kelas',$id_kelas)->get('tbl_kelas')->row_object();
		$data_siswa = $this->db->where('nis',$id_siswa)->get('tbl_siswa')->row_object();
		
		$this->dataParsing['kelas'] = $data->kelas;
		$this->dataParsing['nama'] = $data_siswa->nama;
		$this->dataParsing['data_mapel'] = $this->db->get('tbl_mapel')->result_object();

		$this->dataParsing['data_kelas_lama'] = $this->db->query('select distinct n.id_kelas,k.kelas from tbl_nilai n,tbl_kelas k where n.id_kelas = k.id_kelas')->result_object();

		$this->dataParsing['data'] = $this->M->getAllDataNilai($id_kelas,$id_siswa);
		$this->dataParsing['data2'] = $this->M->getAllDataNilaiLama($id_siswa);
		
		$this->load->view('back_end/edit_nilai',$this->dataParsing);	
	}
	function cetak_nilai_siswa($nis,$id_kelas,$semester,$judul){
		ob_start();
		$this->load->library('html2pdf');

			$this->dataParsing['siswa'] = $this->db->where('nis',$nis)->get('tbl_siswa')->row_object();
			$this->dataParsing['kelas'] = $this->db->where('id_kelas',$id_kelas)->get('tbl_kelas')->row_object();
			$this->dataParsing['semester'] = $semester;
			
			$this->dataParsing['data'] = $this->db->query("select n.*,m.mapel from tbl_nilai n,tbl_mapel m where n.id_mapel = m.id_mapel and n.semester ='".$semester."' and n.nis ='".$nis."' and n.id_kelas ='".$id_kelas."'")->result_object();	
			
			$content=$this->load->view('back_end/report_pdf/laporan_nilai_siswa',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('P', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}

	function getDataNilai($semester,$kelas,$id_kelas){
		$data = $this->db->where('semester',$semester)->where('id_kelas',$kelas)->get('tbl_nilai')->result_object();
		$no=1;
			foreach ($data as $data) {
				if($id_kelas != $data->id_kelas){
					$mapel = $this->db->where('id_mapel',$data->id_mapel)->get('tbl_mapel')->row_object();
					echo '<tr>
							<td>'.$no.'</td>
							<td>'.$mapel->mapel.'</td>
							<td>'.$data->nilai_uts.'</td>
							<td>'.$data->nilai_uas.'</td>
							
						 </tr>';$no++;	
				}
				
			}
			/*<td>
                    			<a href="#" onclick="edit_data('.$data->id_nilai.')" class="btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    			<a href="#" onclick="delete_data('.$data->id_nilai.')" class="delete_data btn btn-danger fa fa-trash" > Delete</a>
                    		</td>*/
	}
	function getDataId($id){
		$data = $this->db->where('id_nilai',$id)->get('tbl_nilai')->row_object();
		echo json_encode($data);
	}
	function update_data(){
		if($this->input->post()){
			
			$this->M->update($this->input->post("id_nilai"),$this->input->post());
			$this->session->set_flashdata("proses","update");
			redirect('C_nilai/edit_nilai/'.$this->input->post("id_kelas")."/".$this->input->post("nis"));	
		}
	}
	function insert_data(){
		if($this->input->post()){
			
			$this->M->insert($this->input->post());
			$this->session->set_flashdata("proses","simpan");
			redirect('C_nilai/edit_nilai/'.$this->input->post("id_kelas")."/".$this->input->post("nis"));	
		}
		
	} 
	function delete_data($id,$id_kelas,$nis){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_nilai/edit_nilai/'.$id_kelas."/".$nis);	
	}

}
