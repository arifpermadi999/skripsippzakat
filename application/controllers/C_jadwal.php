<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_jadwal extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_jadwal','M');
		$this->dataParsing['title'] = "Jadwal";

		$this->dataParsing['menu_active'] = "jadwal";
	}
	public function index()
	{
		$this->dataParsing['data'] = $this->M->getAllDataKelas();
		$this->load->view('back_end/jadwal',$this->dataParsing);
	}
	function detail_jadwal($id_kelas){
		$this->dataParsing['id_kelas'] = $id_kelas; 
		$this->dataParsing['data_jadwal'] = $this->M->getAllData($id_kelas,'Senin','1');
		$this->dataParsing['mapel'] = $this->M->getAllDataMapel();
		$this->dataParsing['waktu'] = $this->M->getAllDataWaktu();
		$this->load->view('back_end/detail_jadwal',$this->dataParsing);	
	}
	function edit_data($id){
		$data =$this->db->where('id_jadwal',$id)->get('tbl_jadwal')->row_object();
		echo json_encode($data);
	}
	function lihat_jadwal_siswa(){
		$db_siswa = $this->db->where('nis',$this->session->userdata('kode'))->get('tbl_siswa')->row_object();
		$db_kelas = $this->db->where('id_kelas',$db_siswa->id_kelas)->get('tbl_kelas')->row_object();
		
		if($db_kelas){
			$this->dataParsing['kelas'] = $db_kelas;
			$this->load->view('back_end/jadwal_siswa',$this->dataParsing);	
		}	
	}
	function lihat_jadwal_guru(){
		$this->load->view('back_end/jadwal_guru',$this->dataParsing);	
		
	}
	function validasi_jadwal(){
		$data_jam = $this->M->valid_jadwal($this->input->post('id_kelas'),$this->input->post('hari'),$this->input->post('semester'),$this->input->post('id_jam'),$this->input->post('id_mapel'));
		$data_mapel = $this->M->valid_jadwal_mapel($this->input->post('id_kelas'),$this->input->post('hari'),$this->input->post('semester'),$this->input->post('id_mapel'));
		$array_data = array();
		if($data_jam){
			$array_data['status'] = "ada";
		}else if($data_mapel){
			$array_data['status'] = "ada_mapel";
		}else{
			$array_data['status'] = "beda";
		}
		echo json_encode($array_data);
                  
	}
	function update_data(){
		
				
			$this->M->update($this->input->post('id_jadwal'),$this->input->post());
			$data = $this->M->getAllData($this->input->post('id_kelas'),$this->input->post('hari'),$this->input->post('semester'));
			$no=1;
			foreach ($data as $data) {
			
					echo '<tr id="data_table'.$data->id_jadwal.'">
							<td>'.$no.'</td>
							<td>'.$data->mapel.'</td>
							<td>'.$data->jam.'</td>
							<td>
                    			<a href="#" onclick="edit_data('.$data->id_jadwal.')" class="btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    			<a href="#" onclick="delete_data('.$data->id_jadwal.')" class="delete_data btn btn-danger fa fa-trash" > Delete</a>
                    		</td>
						 </tr>';$no++;
			}

	}
	function cetak_jadwal_guru($judul){
		ob_start();
		$this->load->library('html2pdf');
			
		$this->dataParsing['data'] = $this->db->where('nip',$this->session->userdata('kode'))->get('tbl_mapel')->result_object();
			
		$content=$this->load->view('back_end/report_pdf/laporan_jadwal_guru',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}

	function cetak_jadwal_siswa($kelas,$semester,$judul){
		ob_start();
		$this->load->library('html2pdf');
			
		$this->dataParsing['kelas'] = $kelas;
		$this->dataParsing['semester'] = $semester;
			
		$content=$this->load->view('back_end/report_pdf/laporan_jadwal_siswa',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}
	public function show_jadwal($id_kelas,$hari,$semester){

			$data = $this->M->getAllData($id_kelas,$hari,$semester);
			$no=1;
			foreach ($data as $data) {
			
					echo '<tr id="data_table'.$data->id_jadwal.'">
							<td>'.$no.'</td>
							<td>'.$data->mapel.'</td>
							<td>'.$data->jam.'</td>
							<td>
                    			<a href="#" onclick="edit_data('.$data->id_jadwal.')" class="btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    			<a href="#" onclick="delete_data('.$data->id_jadwal.')" class="delete_data btn btn-danger fa fa-trash" > Delete</a>
                    		</td>
						 </tr>';$no++;
			}
	}

	public function show_jadwal_siswa($id_kelas,$hari,$semester){

			$data = $this->M->getAllData($id_kelas,$hari,$semester);
			$no=1;
			foreach ($data as $data) {
			
					echo '<tr id="data_table'.$data->id_jadwal.'">
							<td>'.$no.'</td>
							<td>'.$data->mapel.'</td>
							<td>'.$data->jam.'</td>
						 </tr>';$no++;
			}
	}

	public function show_jadwal_guru($hari){
			$data = $this->db->where('nip',$this->session->userdata('kode'))->get('tbl_mapel')->result_object();
			
			$no=1;
			foreach ($data as $data) {
					$data_guru = $this->M->getAllDataByGuru($hari,$data->id_mapel);
					if($data_guru){

					echo '<tr>
							<td>'.$no.'</td>
							<td>'.$data_guru->mapel.'</td>
							<td>'.$data_guru->kelas.'</td>
							<td>'.$data_guru->jam.'</td>
						 </tr>';$no++;

					}
			}
	}


	function insert_data(){
				
			$this->M->insert($this->input->post());
			$data = $this->M->getAllData($this->input->post('id_kelas'),$this->input->post('hari'),$this->input->post('semester'));
			$no=1;
			foreach ($data as $data) {
			
					echo '<tr id="data_table'.$data->id_jadwal.'">
							<td>'.$no.'</td>
							<td>'.$data->mapel.'</td>
							<td>'.$data->jam.'</td>
							<td>
                    			<a href="#" onclick="edit_data('.$data->id_jadwal.')" class="btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    			<a href="#" onclick="delete_data('.$data->id_jadwal.')" class="delete_data btn btn-danger fa fa-trash" > Delete</a>
                    		</td>
						 </tr>';$no++;
			}

		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_jadwal');	
	}

}
