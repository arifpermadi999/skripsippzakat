<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_login extends CI_Controller {

	public function __construct(){
		parent::__construct();
		
	}
	public $dataParsing=array(
		'title' => 'Auto CRUD',
		'logo_title' => 'back_end/component/logo_title',
		'meta' => 'back_end/component/meta',
		'css' => 'back_end/component/css',
		'menu_active' => '',
		'footer' => 'back_end/component/footer',
		'js' => 'back_end/component/js',
		'menu' => 'back_end/component/menu',
		'sidebar_option' => 'back_end/component/sidebar_option',
		'modal_profile' => 'back_end/component/modal_profile',
		'folder_view' =>'back_end/autocrud/',
		'alert' => 'back_end/component/alert',
		);
	public function index()
	{	
		if($this->session->userdata('kode')){
			redirect('BackendController');
		}
		$this->load->view('login',$this->dataParsing);
	}
	function login(){
		$email = $this->input->post('email');
		$password = $this->enkripsi_password($this->input->post('password'));
		

		$login_admin = $this->auth_admin($email,$password);
		echo $login_admin;exit;
		if($login_admin){
			$this->session->set_userdata(array('nama'=>$login_admin,'kode'=>$login_admin,'status'=>'Admin'));
			redirect('BackendController');	
		}
		else{
			$this->session->set_userdata(array('error'=>'error'));
			redirect('C_login');
		}

	}
	function logout(){
		$this->session->sess_destroy();
		   // null the session (just in case):
				$this->session->unset_userdata(array(
					'nama'=>'',
					'kode' =>'',
					'status' => ''
					));
		redirect('C_login');
	}
	
}
