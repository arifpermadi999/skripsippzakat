<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class FrontendController extends MY_frontend {

	public function index()
	{/*
		$this->dataParsing['data'] = $this->M->getAllData();
		*//*
		print_r($this->db->query("SHOW CREATE table t_mustahiq")->row_object());*/
		$this->dataParsing['on_menu'] = "Beranda";


		$this->dataParsing['data_berita'] = $this->M->getAllData("Berita",'desc');
		$this->dataParsing['data'] = $this->M->getAllData("Berita",'desc');
		$this->dataParsing['container'] = "front_end/home";
		$this->load->view('front_end/MainView',$this->dataParsing);
	}

	public function galeri()
	{
		$this->dataParsing['on_menu'] = "Galeri";

		$this->dataParsing['data_berita'] = $this->M->getAllData("Berita",'desc');
		$this->dataParsing['data'] = $this->M->getAllData("Galeri",'desc');
		$this->dataParsing['container'] = "front_end/galeri";
		$this->load->view('front_end/MainView',$this->dataParsing);
	}

	public function detail_galeri($id)
	{
		$this->dataParsing['on_menu'] = "";

		$this->dataParsing['data_berita'] = $this->M->getAllData("Berita",'desc');
		$data = $this->M->getAllDataById("Galeri",$id);
		$this->dataParsing['title_detail'] = "Galeri";
		
		$data_detail['judul'] =$data->judul_galeri;
		$data_detail['keterangan'] =$data->keterangan;
		$data_detail['photo'] =base_url().'asset/Picture/Galeri/'.$data->photo_galeri;
		$data_detail['tanggal'] =$data->tanggal;
		
		$this->dataParsing['data'] = $data_detail;
		
		$this->dataParsing['container'] = "front_end/detail";
		$this->load->view('front_end/MainView',$this->dataParsing);
		}

	public function detail_berita($id)
	{
		$this->dataParsing['on_menu'] = "";

		$this->dataParsing['data_berita'] = $this->M->getAllData("Berita",'desc');
		$data = $this->M->getAllDataById("Berita",$id);
		$this->dataParsing['title_detail'] = "Berita";

		$data_detail['judul'] =$data->judul_berita;
		$data_detail['keterangan'] =$data->isi_berita;
		$data_detail['photo'] =base_url().'asset/Picture/Berita/'.$data->photo_berita;
		$data_detail['tanggal'] =$data->tanggal;
		
		$this->dataParsing['data'] = $data_detail;
		
		$this->dataParsing['container'] = "front_end/detail";
		$this->load->view('front_end/MainView',$this->dataParsing);
	}

	public function pendaftaran()
	{
		$this->dataParsing['on_menu'] = "Pendaftaran";

		$this->dataParsing['data_katmuzakki'] = $this->M->getAllData("Kategori Muzakki");
		$this->dataParsing['data_berita'] = $this->M->getAllData("Berita",'desc');
		$this->dataParsing['auto_id'] = $this->M->auto_id("t_muzakki",'id_muzakki','MZ');
		
		$this->dataParsing['container'] = "front_end/pendaftaran";
		$this->load->view('front_end/MainView',$this->dataParsing);
	}
	public function action_pendaftaran(){
		$table ="t_muzakki";
		$data = $this->input->post();

		if($this->input->post()){

			$this->M->insert($table,$data);

			/*redirect('C_guru');	*/
			$this->session->set_flashdata("proses","pendaftaran");
		
		}			
		redirect('FrontendController/pendaftaran');

	}
	public function login(){

		$this->dataParsing['on_menu'] = "Login";

		$this->dataParsing['container'] = "front_end/login";
		$this->load->view('front_end/MainView',$this->dataParsing);	
	}
	public function profil(){

		$this->dataParsing['on_menu'] = "Profil";

		$this->dataParsing['container'] = "front_end/profil";
		$this->load->view('front_end/MainView',$this->dataParsing);	
	}
	public function edit_profil(){

		$this->dataParsing['on_menu'] = "Profil";
		
		$this->dataParsing['data'] = $this->session->userdata('data');

		$this->dataParsing['data_katmuzakki'] = $this->M->getAllData("Kategori Muzakki");
		$this->dataParsing['container'] = "front_end/edit_profil";
		$this->load->view('front_end/MainView',$this->dataParsing);	
	}
	public function konfirmasi_transfer(){

		$this->dataParsing['on_menu'] = "Profil";
		
		$this->dataParsing['data_rekening'] = $this->M->getAllData("Rekening");
		$this->dataParsing['auto_id'] = $this->M->auto_id("t_konfirmasi",'id_transfer','T');
		$this->dataParsing['container'] = "front_end/konfirmasi_transfer";
		$this->load->view('front_end/MainView',$this->dataParsing);	
	}

	public function action_login(){
		$data = $this->input->post();

		if($this->input->post()){

			$loginData =  $this->M->getLogin($data);
			if($loginData){
				$this->session->set_userdata(array('status'=>'muzakki','data'=>$loginData));
				redirect('FrontendController');
			}else{
				$this->session->set_flashdata("error","username dan password salah");
				redirect('FrontendController/login');
			}
			/*redirect('C_guru');	*/
		
		}			

	}

	public function action_edit_profil(){
		
		$table ="t_muzakki";
		$id_name = "id_muzakki";
		$data  =$this->input->post();

		if($this->input->post()){

			
			$this->update_data($table,$id_name,$data);

			$loginData =  $this->M->getLogin($data);

			$this->session->set_userdata(array('data'=>$loginData));
			/*redirect('C_guru');	*/
			$this->session->set_flashdata("proses","edit_profil");
		
		}			
		redirect('FrontendController/edit_profil');

	}

	function logout(){

		$this->session->sess_destroy();
		$this->session->unset_userdata(array(
					'data'=>'',
					'status' => ''
					));
		redirect('FrontendController');
		
	}
	
	public function action_konfirmasi_transfer(){
		$path = "./asset/Picture/Bukti_Transfer/";
		$this->configImage($path);
		$image_src="";
		
		if($_FILES['bukti_transfer']['name']){
			if($this->upload->do_upload("bukti_transfer"))
			{					
				$fileinfo = $this->upload->data();					
				$image_src =$fileinfo['file_name'];
			}
			else
			{
				$this->session->set_userdata(array('proses'=>'error'));
				$this->session->set_userdata(array('error'=>$this->upload->display_errors()));
				print_r($this->upload->display_errors());
			} 		
		}


		$data = $this->input->post();

		$data['tgl_transfer'] = date('Y-m-d H:i:s'); 
		
		if($image_src){

			$data['bukti_transfer'] = $image_src;	
		}

		$this->insert_data("t_konfirmasi",$data);		
		
		$this->session->set_flashdata("proses","konfirmasi_transfer");
		
		redirect('FrontendController/konfirmasi_transfer');
		
	}

	public function riwayat_transfer(){
		
		$this->dataParsing['on_menu'] = "Profil";
		
		$this->dataParsing['data'] = $this->M->getAllData("RiwayatTransfer");
		$this->dataParsing['container'] = "front_end/riwayat_konfirmasi_transfer";
		$this->load->view('front_end/MainView',$this->dataParsing);	
	}

}
