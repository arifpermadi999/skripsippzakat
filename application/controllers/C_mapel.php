<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_mapel extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_mapel','M');
		$this->dataParsing['title'] = "Mapel";

		$this->dataParsing['menu_active'] = "mapel";
	}
	public function index()
	{
		$this->dataParsing['guru'] = $this->M->getAllDataGuru();
		$this->dataParsing['data'] = $this->M->getAllData();
		$this->load->view('back_end/mapel',$this->dataParsing);
	}
	function getDataId($id){
                  
		$data = $this->M->getAllDataById($id);
		
		echo json_encode($data);
	}
	function validasi_id($id){
		$data = $this->db->where('id_mapel',$id)->get('tbl_mapel')->row_object();
		$data_array = array();
		if($data){
			$data_array['validasi'] = "sama";
		}else{
			$data_array['validasi'] = "beda";
		}
		echo json_encode($data_array);
	}
	function update_data(){
		if($this->input->post('submit')){
			
			$data = array(
					'mapel' => $this->input->post("mapel"),
					'nip' => $this->input->post("nip"),
				);
			$this->M->update($this->input->post("id_mapel"),$data);
			$this->session->set_flashdata("proses","update");
			redirect('C_mapel');	
		}
	}
	function insert_data(){
		if($this->input->post()){
			
			$data = array(
					'id_mapel' => $this->input->post("id_mapel"),
					'mapel' => $this->input->post("mapel"),
					'nip' => $this->input->post("nip"),
				);
			$this->M->insert($data);
			$this->session->set_flashdata("proses","simpan");
			redirect('C_mapel');	
		}
		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_mapel');	
	}

}
