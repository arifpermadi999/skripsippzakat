<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class BackendController extends MY_backend {

	public function index()
	{
		$this->dataParsing['title'] = "Dashboard";
		$this->load->view('dashboard',$this->dataParsing);
	}
	function search(){
		$search = $this->input->post("searchGoogle");
		//echo $search;
		header("location:https://www.google.com/search?client=opera&q=".$search."&sourceid=opera&ie=UTF-8&oe=UTF-8");
	}
	
	public function muzakki($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Muzakki";
		$this->dataParsing['on_menu'] = "BackendController/muzakki";

		$title = $this->dataParsing['title'];
		$table ="t_muzakki";
		$id_name = "id_muzakki";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['data_katmuzakki'] = $this->M->getAllData("Kategori Muzakki");
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/muzakki",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}


	}


	public function mustahiq($event = NULL,$id = NULL){

		
		$this->dataParsing['title'] = "Mustahiq";
		$this->dataParsing['on_menu'] = "BackendController/mustahiq";

		$title = $this->dataParsing['title'];
		$table ="t_mustahiq";
		$id_name = "id_mustahiq";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['data_katmustahiq'] = $this->M->getAllData("Kategori Mustahiq");
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/mustahiq",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}


	}
	public function kat_muzakki($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Kategori Muzakki";
		$this->dataParsing['on_menu'] = "BackendController/kat_muzakki";

		$title = $this->dataParsing['title'];
		$table ="t_katmuzakki";
		$id_name = "id_katmuzakki";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/kat_muzak",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}

	public function kat_mustahiq($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Kategori Mustahiq";
		$this->dataParsing['on_menu'] = "BackendController/kat_mustahiq";

		$title = $this->dataParsing['title'];
		$table ="t_katmustahiq";
		$id_name = "id_katmustahiq";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/kat_mustahiq",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}
	public function program_zis($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Program ZIS";
		$this->dataParsing['on_menu'] = "BackendController/program_zis";

		$title = $this->dataParsing['title'];
		$table ="t_program";
		$id_name = "id_program";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['auto_id'] = $this->M->auto_id("t_program",'id_program','PZ');
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/program_zis",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}

	public function saldo($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Saldo";
		$this->dataParsing['on_menu'] = "BackendController/saldo";

		$title = $this->dataParsing['title'];
		$table ="t_saldo";
		$id_name = "id_saldo";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){
			$data = $this->input->post();
			$data['jumlah_saldo'] = $data['saldo'];
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['auto_id'] = $this->M->auto_id("t_saldo",'id_saldo','SD');
			$this->dataParsing['data'] = $this->M->getAllData($title);
			foreach ($this->dataParsing['data'] as $value) {
				$penyalur = $this->db->select('sum(jumlah_salur) as jumlah_salur')->where('id_saldo',$value->id_saldo)->get('t_penyaluran_zis')->row_object();
				$penerima = $this->db->select('sum(jumlah_terima) as jumlah_terima')->where('id_saldo',$value->id_saldo)->get('t_penerimaan_zis')->row_object();
				$saldo = ((int)$value->saldo + (int)$penerima->jumlah_terima) - (int)$penyalur->jumlah_salur;

				$this->dataParsing['data'][0]->jumlah_saldo = $saldo;
			}
			$this->load->view("back_end/saldo",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}
	public function penerimaan_zis($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Penerimaan ZIS";
		$this->dataParsing['on_menu'] = "BackendController/penerimaan_zis";

		$title = $this->dataParsing['title'];
		$table ="t_penerimaan_zis";
		$id_name = "id_penerimaan_zis";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$zis = $this->db->where('id_penerimaan_zis',$id)->get('t_penerimaan_zis')->row_object();

			$data = ['id_transfer'=>$zis->id_transfer,'status_konfirmasi'=>1];	
			$this->update_data("t_konfirmasi",'id_transfer',$data);

			$this->delete_data($table,$id_name,$id);

		}else if($event == "insert_data"){
			//update transfer
			$data = ['id_transfer'=>$this->input->post('id_transfer'),'status_konfirmasi'=>2];	
			$this->update_data("t_konfirmasi",'id_transfer',$data);

			
			//update Saldo
			/*$saldo = $this->db->where('id_saldo',$this->input->post('id_saldo'))->get('t_saldo')->row_object();
			$up_saldo = $saldo->jumlah_saldo + (int)$this->input->post('jumlah_terima');
			
			$data = ['id_saldo'=>$this->input->post('id_saldo'),'jumlah_saldo' => $up_saldo];	
			$this->update_data("t_saldo",'id_transfer',$data);
			*/
			//insetrt penerimaan zis
			$data = $this->input->post();

			unset($data['no_transfer2']);

			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['auto_id'] = $this->M->auto_id("t_penerimaan_zis",'id_penerimaan_zis','PRZ');
			
			$this->dataParsing['data_program'] = $this->M->getAllData("Program ZIS");
			$this->dataParsing['data_muzakki'] = $this->M->getAllData("Muzakki");
			$this->dataParsing['data_saldo'] = $this->M->getAllData("Saldo");

			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/penerimaan_zis",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}
	public function penyaluran_zis($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Penyaluran ZIS";
		$this->dataParsing['on_menu'] = "BackendController/penyaluran_zis";

		$title = $this->dataParsing['title'];
		$table ="t_penyaluran_zis";
		$id_name = "id_penyaluran_zis";

		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$zis = $this->db->where('id_penerimaan_zis',$id)->get('t_penerimaan_zis')->row_object();
						
			$data = ['id_transfer'=>$zis->id_transfer,'status_konfirmasi'=>1];	
			$this->update_data("t_konfirmasi",'id_transfer',$data);

			$this->delete_data($table,$id_name,$id);

		}else if($event == "insert_data"){

			
			//update Saldo
			/*$saldo = $this->db->where('id_saldo',$this->input->post('id_saldo'))->get('t_saldo')->row_object();
			$up_saldo = $saldo->jumlah_saldo + (int)$this->input->post('jumlah_terima');
			
			$data = ['id_saldo'=>$this->input->post('id_saldo'),'jumlah_saldo' => $up_saldo];	
			$this->update_data("t_saldo",'id_transfer',$data);
			*/
			//insetrt penerimaan zis
			$data = $this->input->post();

			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['auto_id'] = $this->M->auto_id($table,$id_name,'PNZ');
			
			$this->dataParsing['data_program'] = $this->M->getAllData("Program ZIS");
			$this->dataParsing['data_mustahiq'] = $this->M->getAllData("Mustahiq");
			$this->dataParsing['data_saldo'] = $this->M->getAllData("Saldo");

			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/penyaluran_zis",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}

	public function rekening($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Rekening";
		$this->dataParsing['on_menu'] = "BackendController/rekening";

		$title = $this->dataParsing['title'];
		$table ="t_rekening";
		$id_name = "id_t_rekening";


		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();
			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['auto_id'] = $this->M->auto_id("t_rekening",'id_t_rekening','RK');
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/rekening",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}
	public function post_berita($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Berita";
		$this->dataParsing['on_menu'] = "BackendController/post_berita";

		$title = $this->dataParsing['title'];
		$table ="t_berita";
		$id_name = "id_berita";

		if($event == "insert_data" || $event == "update_data"){
			$path = "./asset/Picture/Berita/";
			$this->configImage($path);
			$image_src="";
			
			if($_FILES['photo_berita']['name']){
				if($this->upload->do_upload("photo_berita"))
				{					
					$fileinfo = $this->upload->data();					
					$image_src =$fileinfo['file_name'];
				}
				else
				{
					$this->session->set_userdata(array('proses'=>'error'));
					$this->session->set_userdata(array('error'=>$this->upload->display_errors()));
					print_r($this->upload->display_errors());
				} 		
			}

		}



		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();

			$data['tanggal'] = date('Y-m-d H:i:s'); 
			
			unset($data['_wysihtml5_mode']);
			
			if($image_src){

				$data['photo_berita'] = $image_src;	
			}

			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();

			$data['tanggal'] = date('Y-m-d H:i:s'); 
			
			unset($data['_wysihtml5_mode']);
			
			if($image_src){

				$data['photo_berita'] = $image_src;	
			}

			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/berita",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}

	}
	public function post_galeri($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Galeri";
		$this->dataParsing['on_menu'] = "BackendController/post_galeri";

		$title = $this->dataParsing['title'];
		$table ="t_galeri";
		$id_name = "id_galeri";

		if($event == "insert_data" || $event == "update_data"){
			$path = "./asset/Picture/Galeri/";
			$this->configImage($path);
			$image_src="";
			
			if($_FILES['photo_galeri']['name']){
				if($this->upload->do_upload("photo_galeri"))
				{					
					$fileinfo = $this->upload->data();					
					$image_src =$fileinfo['file_name'];
				}
				else
				{
					$this->session->set_userdata(array('proses'=>'error'));
					$this->session->set_userdata(array('error'=>$this->upload->display_errors()));
					print_r($this->upload->display_errors());
				} 		
			}

		}



		if($event == "validasi_id"){

			$this->validasi_id($title,$id);

		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else if($event == "update_data"){
			
			$data = $this->input->post();

			$data['tanggal'] = date('Y-m-d H:i:s'); 
			
			unset($data['_wysihtml5_mode']);
			
			
			if($image_src){

				$data['photo_galeri'] = $image_src;	
			}
			
			$this->update_data($table,$id_name,$data);

		}else if($event == "insert_data"){

			$data = $this->input->post();

			$data['tanggal'] = date('Y-m-d H:i:s'); 
			
			unset($data['_wysihtml5_mode']);
			
			if($image_src){

				$data['photo_galeri'] = $image_src;	
			}

			$this->insert_data($table,$data);			

		}else{
			$this->dataParsing['data'] = $this->M->getAllData($title);
			$this->load->view("back_end/galeri",$this->dataParsing);
		}

		if($event == "insert_data" || $event == "update_data" || $event == "delete_data"){
			redirect($this->dataParsing['on_menu']);	
		}


	}

	function konfirmasi_transfer($event = NULL,$id = NULL){

		$this->dataParsing['title'] = "Konfirmasi Transfer";
		$this->dataParsing['on_menu'] = "BackendController/konfirmasi_transfer";
		$table ="t_konfirmasi";
		$id_name = "id_transfer";

		$title = $this->dataParsing['title'];

		if($event == "konfirmasi"){
			
			$data = ['status_konfirmasi'=>1];

			$this->M->update($table,$id_name,$id,$data);

			redirect('BackendController/konfirmasi_transfer');
		
		}else if($event == "delete_data"){

			$this->delete_data($table,$id_name,$id);

		}else{
			$this->dataParsing['data'] = $this->M->getAllData("RiwayatTransfer2");
			
			$this->load->view("back_end/konfirmasi_transfer",$this->dataParsing);	
		}
	}
	function getTransferMuzakkiJson($id){

			$data_transfer = $this->M->getAllDataById("TransferMuzakki",$id);
			echo json_encode($data_transfer);
	}	


}
