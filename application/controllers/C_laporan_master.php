<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_laporan_master extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->dataParsing['title'] = "Laporan Master";
		$this->dataParsing['menu_active'] = "laporan_master";
	}
	public function index()
	{
		$this->dataParsing['kelas'] = $this->db->get('tbl_kelas')->result_object();
		$this->dataParsing['guru'] = $this->db->get('tbl_guru')->result_object();
		$this->load->view('back_end/laporan_master',$this->dataParsing);
	}
	function laporan_siswa($value,$status_cetak,$judul){
		ob_start();
		$this->load->library('html2pdf');

			if($status_cetak == "1"){
				$this->dataParsing['status'] = "Aktif";
				$this->dataParsing['kelas'] = "";
				$this->dataParsing['data'] = $this->db->query("select s.*,k.kelas from tbl_siswa s,tbl_kelas k where s.id_kelas = k.id_kelas and s.status ='Aktif'")->result_object();
			}else if($status_cetak == "0"){
				$this->dataParsing['status'] = "Tidak Aktif";
				$this->dataParsing['kelas'] = "";
				$this->dataParsing['data'] = $this->db->query("select s.*,k.kelas from tbl_siswa s,tbl_kelas k where s.id_kelas = k.id_kelas and s.status ='Tidak Aktif'")->result_object();	

			}else if($status_cetak == "kelas"){
				$this->dataParsing['status'] = "Per Kelas";

				$kelas = $this->db->where('id_kelas',$value)->get('tbl_kelas')->row_object();
				$this->dataParsing['kelas'] = $kelas->kelas;
				$this->dataParsing['data'] = $this->db->query("select s.*,k.kelas from tbl_siswa s,tbl_kelas k where s.id_kelas = k.id_kelas and s.id_kelas = '".$value."'")->result_object();	
			}
			
			$content=$this->load->view('back_end/report_pdf/laporan_master_siswa',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}
	function laporan_kelas($judul){
		ob_start();
		$this->load->library('html2pdf');

			$this->dataParsing['data'] = $this->db->query('select m.*,g.nama from tbl_kelas m,tbl_guru g where m.nip = g.nip')->result_object();
			
			$content=$this->load->view('back_end/report_pdf/laporan_master_kelas',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
		
	}
	function laporan_mapel($judul){
		ob_start();
		$this->load->library('html2pdf');

			$this->dataParsing['guru'] = "";

			$this->dataParsing['data'] = $this->db->query('select m.*,g.nama from tbl_mapel m,tbl_guru g where m.nip = g.nip')->result_object();
			
			$content=$this->load->view('back_end/report_pdf/laporan_master_mapel',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}

	function laporan_mapel_guru($id_guru,$judul){
		ob_start();
		$this->load->library('html2pdf');

			$this->dataParsing['data'] = $this->db->query("select m.*,g.nama from tbl_mapel m,tbl_guru g where m.nip = g.nip and m.nip = '".$id_guru."'")->result_object();
			$guru = $this->db->where('nip',$id_guru)->get('tbl_guru')->row_object();
			$this->dataParsing['guru'] = $guru->nama; 
			
			$content=$this->load->view('back_end/report_pdf/laporan_master_mapel',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}

	function laporan_guru(){
		ob_start();
		$this->load->library('html2pdf');

			$this->dataParsing['data'] = $this->db->get('tbl_guru')->result_object();

			
			$content=$this->load->view('back_end/report_pdf/laporan_master_guru',$this->dataParsing);
		

			$content = ob_get_clean();		
			ob_end_clean();
			try
			{
				$html2pdf = new HTML2PDF('L', 'A4', 'fr');
				$html2pdf->pdf->SetDisplayMode('fullpage');
				$html2pdf->writeHTML($content, isset($_GET['vuehtml']));
				$html2pdf->Output('print.pdf');
			}
			catch(HTML2PDF_exception $e) {
				echo $e;
				exit;
			}
	}
	
}
