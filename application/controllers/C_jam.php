<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_jam extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_jam','M');
		$this->dataParsing['title'] = "Jam";
		$this->dataParsing['menu_active'] = "jam";
	}
	public function index()
	{
		$this->dataParsing['data'] = $this->M->getAllData();
		$this->load->view('back_end/jam',$this->dataParsing);
	}
	function getDataId($id){
                  
		$data = $this->M->getAllDataById($id);
		
		echo json_encode($data);
	}
	function update_data(){
		if($this->input->post('submit')){
			
			$data = array(
					'jam' => $this->input->post("jam"),
				);
			$this->M->update($this->input->post("id_jam"),$data);
			$this->session->set_flashdata("proses","update");
			redirect('C_jam');	
		}
	}
	function insert_data(){
		if($this->input->post('submit')){
			
			$jam_mulai = $this->input->post('jam_mulai');
			$jam_akhir = $this->input->post('jam_akhir');
			$jam = $jam_mulai." - ".$jam_akhir;
			$data = array(
					'jam' => $jam,
				);
			$this->M->insert($data);
			$this->session->set_flashdata("proses","simpan");
			redirect('C_jam');	
		}
		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_jam');	
	}

}
