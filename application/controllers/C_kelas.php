<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_kelas extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_kelas','M');
		$this->dataParsing['title'] = "Kelas";

		$this->dataParsing['menu_active'] = "kelas";
	}
	public function index()
	{
		$this->dataParsing['guru'] = $this->M->getAllDataGuru();
		$this->dataParsing['data'] = $this->M->getAllData();
		$this->load->view('back_end/kelas',$this->dataParsing);
	}
	function getDataId($id){
                  
		$data = $this->M->getAllDataById($id);
		
		echo json_encode($data);
	}
	function update_data(){
		if($this->input->post('submit')){
		
			$data = array(
					'kelas' => $this->input->post("kelas"),
					'nip' => $this->input->post("nip"),
				);
			$this->M->update($this->input->post("id_kelas"),$data);
			$this->session->set_flashdata("proses","update");
			redirect('C_kelas');	
		}
	}
	function insert_data(){
		if($this->input->post()){
		
			$data = array(
					'kelas' => $this->input->post("kelas"),
					'nip' => $this->input->post("nip"),
				);
			$this->M->insert($data);
			$this->session->set_flashdata("proses","simpan");
			redirect('C_kelas');	
		}
		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_kelas');	
	}

}
