<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_dashboard extends MY_backend {

	public function __construct(){
		parent::__construct();
		
		$this->dataParsing['menu_active'] = "dashboard";
		
	}
	public function index()
	{
		$this->load->view('dashboard',$this->dataParsing);
        
	}
	
	
}
