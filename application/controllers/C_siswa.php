<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_siswa extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_siswa','M');
		$this->dataParsing['title'] = "Siswa";
		$this->dataParsing['title'] = "siswa";
	}
	public function index()
	{
		$this->dataParsing['data'] = $this->M->getAllData();
		$this->dataParsing['data_kelas'] = $this->M->getAllDataKelas();
		$this->load->view('back_end/siswa',$this->dataParsing);
	}

	function validasi_id($id){
		$data = $this->db->where('nis',$id)->get('tbl_siswa')->row_object();
		$data_array = array();
		if($data){
			$data_array['validasi'] = "sama";
		}else{
			$data_array['validasi'] = "beda";
		}
		echo json_encode($data_array);
	}
	function getDataId($id){
                  
		$data = $this->M->getAllDataById($id);
		$pasword = $this->dekripsi_pw($data->password);
		$data_array = array(
					'nis' => $data->nis,
					'nama' => $data->nama,
					'id_kelas' => $data->id_kelas,
					'ttl' => $data->ttl,
					'jenis_kelamin' => $data->jenis_kelamin,
					'tahun_ajar' => $data->tahun_ajar,					
					'agama' => $data->agama,
					'status' => $data->status,
					'keterangan' => $data->keterangan,
					'email' => $data->email,
					'password' => $pasword,
			);
		echo json_encode($data_array);
	}
	function update_data(){
		if($this->input->post('submit')){
			
			$pasword = $this->enkripsi_password($this->input->post('password'));;
					
			$data = array(
					'nama' => $this->input->post("nama"),
					'id_kelas' => $this->input->post("id_kelas"),
					'ttl' => $this->input->post("ttl"),
					'jenis_kelamin' => $this->input->post("jenis_kelamin"),
					
					'agama' => $this->input->post("agama"),
					'status' => $this->input->post("status"),
					'keterangan' => $this->input->post("keterangan"),
					'email' => $this->input->post("email"),
					'password' => $pasword,
				);
			$this->M->update($this->input->post("nis"),$data);
			$this->session->set_flashdata("proses","update");
			redirect('C_siswa');	
		}
	}
	function insert_data(){
		if($this->input->post()){
			$pasword = $this->enkripsi_password($this->input->post('password'));
					
			$data = array(
					'nis' => $this->input->post("nis"),
					'nama' => $this->input->post("nama"),
					'id_kelas' => $this->input->post("id_kelas"),
					'ttl' => $this->input->post("ttl"),
					'jenis_kelamin' => $this->input->post("jenis_kelamin"),
					'tahun_ajar' => $this->input->post("tahun_ajar"),					
					'agama' => $this->input->post("agama"),
					'status' => $this->input->post("status"),
					'keterangan' => $this->input->post("keterangan"),
					'email' => $this->input->post("email"),
					'password' => $pasword,
				);
			$this->M->insert($data);
			$this->session->set_flashdata("proses","simpan");
			redirect('C_siswa');	
		}
		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_siswa');	
	}
	function aktifasi($status,$id){
		$status_db = "";
		if($status == "1"){
			$status_db ="Aktif";
		}else{
			$status_db ="Tidak Aktif";

		} 
		$this->session->set_flashdata("proses","aktif");
		$this->db->where('nis',$id)->update('tbl_siswa',array('status'=>$status_db));
	}

}
