<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_guru extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_guru','M');
		$this->dataParsing['title'] = "Guru";
		$this->dataParsing['menu_active'] = "guru";
	}
	public function index()
	{
		$this->dataParsing['data'] = $this->M->getAllData();
		$this->load->view('back_end/guru',$this->dataParsing);
	}

	function validasi_id($id){
		$data = $this->db->where('nip',$id)->get('tbl_guru')->row_object();
		$data_array = array();
		if($data){
			$data_array['validasi'] = "sama";
		}else{
			$data_array['validasi'] = "beda";
		}
		echo json_encode($data_array);
	}
	
	function getDataId($id){
                  
		$data = $this->M->getAllDataById($id);
		$pasword = $this->dekripsi_pw($data->password);
		$data_array = array(
					'nip' => $data->nip,
					'nama' => $data->nama,
					'ttl' => $data->ttl,
					'jenis_kelamin' => $data->jenis_kelamin,
					'telp' => $data->telp,
					'jabatan' => $data->jabatan,
					'agama' => $data->agama,
					'alamat' => $data->alamat,
					'email' => $data->email,
					'password' => $pasword,
			);
		echo json_encode($data_array);
	}
	function update_data(){
		if($this->input->post('submit')){
			
					$pasword = $this->enkripsi_password($this->input->post('password'));
					
			$data = array(
					'nama' => $this->input->post("nama"),
					'ttl' => $this->input->post("ttl"),
					'jenis_kelamin' => $this->input->post("jenis_kelamin"),
					'telp' => $this->input->post("telp"),
					'jabatan' => $this->input->post("jabatan"),
					'agama' => $this->input->post("agama"),
					'alamat' => $this->input->post("alamat"),
					'email' => $this->input->post("email"),
					'password' => $pasword,
				);
			$this->M->update($this->input->post("nip"),$data);
			$this->session->set_flashdata("proses","update");
			redirect('C_guru');	
		}
	}
	function insert_data(){
		if($this->input->post()){
			$pasword = $this->enkripsi_password($this->input->post('password'));
					
			$data = array(
					'nip' => $this->input->post("nip"),
					'nama' => $this->input->post("nama"),
					'ttl' => $this->input->post("ttl"),
					'jenis_kelamin' => $this->input->post("jenis_kelamin"),
					'telp' => $this->input->post("telp"),
					'jabatan' => $this->input->post("jabatan"),
					'agama' => $this->input->post("agama"),
					'alamat' => $this->input->post("alamat"),
					'email' => $this->input->post("email"),
					'password' => $pasword,
				);
			$this->M->insert($data);
			$this->session->set_flashdata("proses","simpan");
			redirect('C_guru');	
		}
		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_guru');	
	}

}
