<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class C_admin extends MY_backend {

	public function __construct(){
		parent::__construct();
		$this->load->model('M_admin','M');
		$this->dataParsing['title'] = "admin";
	}
	public function index()
	{
		$this->dataParsing['data'] = $this->M->getAllData();
		$this->dataParsing['data_kelas'] = $this->M->getAllDataKelas();
		$this->load->view('back_end/admin',$this->dataParsing);
	}

	function validasi_id($id){
		$data = $this->db->where('nis',$id)->get('tbl_admin')->row_object();
		$data_array = array();
		if($data){
			$data_array['validasi'] = "sama";
		}else{
			$data_array['validasi'] = "beda";
		}
		echo json_encode($data_array);
	}
	public function dekripsi_pw($pw){
		$pass=substr( $pw ,15);
                        $len_pass=strlen($pass);
                        $password="";
                        $panjang="";
                          for($i=0;$i<$len_pass;$i++){
                            
                            if($i==0){
                              $panjang+=3;
                              $password.=substr($pass,$i,1);
                            }
                            else if($i==$panjang){

                              $panjang+=3;
                              $password.=substr($pass,$i,1);
                            }
                            
                          }
                          

                $pasword_value = $password;
                return $pasword_value;
	}
	function getDataId($id){
                  
		$data = $this->M->getAllDataById($id);
		$pasword = $this->dekripsi_pw($data->password);
		$data_array = array(
					'nis' => $data->nis,
					'nama' => $data->nama,
					'id_kelas' => $data->id_kelas,
					'ttl' => $data->ttl,
					'jenis_kelamin' => $data->jenis_kelamin,
					'tahun_ajar' => $data->tahun_ajar,					
					'agama' => $data->agama,
					'status' => $data->status,
					'keterangan' => $data->keterangan,
					'email' => $data->email,
					'password' => $pasword,
			);
		echo json_encode($data_array);
	}
	function update_data(){
		if($this->input->post('submit')){
			$pasworddd = $this->input->post('password'); 
						$pw=strlen($pasworddd);
						$field=substr(md5($pasworddd),0,15);
						//membuat password 15 character md5 1 asli 2 md5
						
						for($i=0;$i<$pw;$i++)
						{
							$field.=substr($pasworddd,$i,1);

							$field.=substr(
										md5( substr($pasworddd,$i,1) )
										,0,2);
						}

					$pasword = $field;
					
			$data = array(
					'nama' => $this->input->post("nama"),
					'id_kelas' => $this->input->post("id_kelas"),
					'ttl' => $this->input->post("ttl"),
					'jenis_kelamin' => $this->input->post("jenis_kelamin"),
					
					'agama' => $this->input->post("agama"),
					'status' => $this->input->post("status"),
					'keterangan' => $this->input->post("keterangan"),
					'email' => $this->input->post("email"),
					'password' => $pasword,
				);
			$this->M->update($this->input->post("nis"),$data);
			$this->session->set_flashdata("proses","update");
			redirect('C_admin');	
		}
	}
	function insert_data(){
		if($this->input->post()){
			$pasworddd = $this->input->post('password'); 
						$pw=strlen($pasworddd);
						$field=substr(md5($pasworddd),0,15);
						//membuat password 15 character md5 1 asli 2 md5
						
						for($i=0;$i<$pw;$i++)
						{
							$field.=substr($pasworddd,$i,1);

							$field.=substr(
										md5( substr($pasworddd,$i,1) )
										,0,2);
						}

					$pasword = $field;
					
			$data = array(
					'nis' => $this->input->post("nis"),
					'nama' => $this->input->post("nama"),
					'id_kelas' => $this->input->post("id_kelas"),
					'ttl' => $this->input->post("ttl"),
					'jenis_kelamin' => $this->input->post("jenis_kelamin"),
					'tahun_ajar' => $this->input->post("tahun_ajar"),					
					'agama' => $this->input->post("agama"),
					'status' => $this->input->post("status"),
					'keterangan' => $this->input->post("keterangan"),
					'email' => $this->input->post("email"),
					'password' => $pasword,
				);
			$this->M->insert($data);
			$this->session->set_flashdata("proses","simpan");
			redirect('C_admin');	
		}
		
	} 
	function delete_data($id){
		$this->M->delete($id);
		$this->session->set_flashdata("proses","delete");
		redirect('C_admin');	
	}
	function aktifasi($status,$id){
		$status_db = "";
		if($status == "1"){
			$status_db ="Aktif";
		}else{
			$status_db ="Tidak Aktif";

		} 
		$this->session->set_flashdata("proses","aktif");
		$this->db->where('nis',$id)->update('tbl_admin',array('status'=>$status_db));
	}

}
