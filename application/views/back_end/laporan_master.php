<!DOCTYPE html>
<html>
  <head>
  
    <title>Laporan Master</title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>
  <style type="text/css">
  .small-box-footer{
    margin-top: 50px;
  }
  </style>
  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Laporan Master
        </h1>
        <ol class="breadcrumb">
        <li class="active">Laporan Master</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Master Siswa</h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                    <!-- Small boxes (Stat box) -->
                    <div class="col-lg-6 col-md-6">
                      <!-- small box -->
                      <div class="small-box bg-green" >
                        <div class="inner">
                          <h3 style="font-size:29px;">Siswa Aktif</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-users"></i>
                        </div>
                        <a href="<?php echo site_url('C_laporan_master/laporan_siswa/1/1/LaporanMasterSiswa'); ?>" class="small-box-footer">Lihat Laporan<i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>

                    <!-- ./col -->
                    <div class="col-lg-6 col-md-6">
                      <!-- small box -->
                      <br>
                      <center><label>Kelas</label></center>
                      <select id="kelas_siswa" class="form-control">
                        <?php foreach($kelas as $data_db){?>
                          <option value="<?php echo $data_db->id_kelas ?>"><?php echo $data_db->kelas ?></option>
                        <?php } ?>
                      </select>
                      <br>
                      <a href="<?php echo site_url('C_laporan_master/laporan_siswa')?>" class="btn btn-info" style="width: 100%;" id="cetak_siswa_kelas">Cetak Laporan Berdasarkan Kelas</a>
                    </div>
                    <br>
                    <!-- ./col -->
                    <div class="col-lg-12 col-md-12">
                      <!-- small box -->
                      <div class="small-box bg-red" style="width: 50%;">
                        <div class="inner">
                          <h3 style="font-size:29px;">Siswa NonAktif</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-users"></i>
                        </div>
                        <a href="<?php echo site_url('C_laporan_master/laporan_siswa/0/0/LaporanMasterSiswa'); ?>" class="small-box-footer">Lihat Laporan <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->
          
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-md-12">
        <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Master Kelas & Guru</h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                    <!-- Small boxes (Stat box) -->
                    <div class="col-lg-6 col-md-6">
                      <!-- small box -->
                      <div class="small-box bg-blue" >
                        <div class="inner">
                          <h3 style="font-size:29px;">Kelas</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-bank"></i>
                        </div>
                        <a href="<?php echo site_url('C_laporan_master/laporan_kelas/LaporanMasterKelas'); ?>" class="small-box-footer">Lihat Laporan<i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-md-6">
                      <!-- small box -->
                      <div class="small-box bg-purple">
                        <div class="inner">
                          <h3 style="font-size:29px;">Guru</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-users"></i>
                        </div>
                        <a href="<?php echo site_url('C_laporan_master/laporan_guru/LaporanMasterGuru'); ?>" class="small-box-footer">Lihat Laporan <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>

              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            </div>
            <!-- /.box-footer-->
          </div>
          
      </div>
      <div class="col-lg-6 col-md-6 col-sm-12 col-md-12">
          <div class="box box-primary">
              <div class="box-header with-border">
                <h3 class="box-title">Mata Pelajaran</h3>

                <div class="box-tools pull-right">
                  
                  <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
                </div>
              </div>
              <div class="box-body">
                <div class="row">

                      <!-- ./col -->
                      <div class="col-lg-6 col-md-6">
                        <!-- small box -->
                        <div class="small-box bg-orange">
                          <div class="inner">
                            <h3 style="font-size:29px;">Mata Pelajaran</h3>
                          </div>
                          <div class="icon">
                            <i class="fa fa-fw fa-book"></i>
                          </div>
                          <a href="<?php echo site_url('C_laporan_master/laporan_mapel/LaporanMasterMapel'); ?>" class="small-box-footer">Lihat Laporan <i class="fa fa-arrow-circle-right"></i></a>
                        </div>
                      </div>

                      <!-- ./col -->
                      <div class="col-lg-6 col-md-6">
                        <!-- small box -->
                        <br>
                        <center><label>Guru</label></center>
                        <select id="guru_mapel" class="form-control">
                          <?php foreach($guru as $data_db){?>
                            <option value="<?php echo $data_db->nip ?>"><?php echo $data_db->nama ?></option>
                          <?php } ?>
                        </select>
                        <br>
                        <a href="<?php echo site_url('C_laporan_master/laporan_mapel_guru')?>" class="btn btn-info" style="width: 100%;" id="cetak_mapel_guru">Cetak Laporan Mapel Berdasarkan Guru</a>
                      </div>
                </div>

              </div>
              <!-- /.box-body -->
              <div class="box-footer">
              </div>
              <!-- /.box-footer-->
            </div>
            <!-- /.box -->
       </div>
    </div>
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
   <script type="text/javascript">
    $(document).ready(function(){

      
       $("#cetak_mapel_guru").click(function(e){
        e.preventDefault();
        var id_mapel = $("#guru_mapel").val();
        if(id_mapel){
          var link = $(this).attr("href") + "/" + id_mapel + "/LaporanMasterMapel";
          window.location.href = link;
        }else{
          swal({
                      title: "Peringatan",
                      text: "Kelas Belum di pilih!",
                      type: "warning"
                  
                  });
        }
      })
      $("#cetak_siswa_kelas").click(function(e){
        e.preventDefault();
        var id_kelas = $("#kelas_siswa").val();
        if(id_kelas){
          var link = $(this).attr("href") + "/" + id_kelas + "/kelas/LaporanMasterSiswa";
          window.location.href =link;
        }else{
          swal({
                      title: "Peringatan",
                      text: "Kelas Belum di pilih!",
                      type: "warning"
                  
                  });
        }
      })
    })
    
    

  </script>

  <?php $this->load->view($modal_profile) ?>

</body>
</html>
