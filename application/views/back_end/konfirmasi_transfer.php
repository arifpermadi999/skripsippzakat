<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title; ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>
                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Tgl Transfer</th>
                <th>Rekening Pentransfer</th>
                <th>Rekening Tujuan</th>
                <th>Jumlah Transfer</th>
                <th>Bukti Transfer</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td ><?php echo $data_db->tgl_transfer; ?></td>
                  <td ><?= $data_db->no_rekening_transfer." - ".$data_db->atas_nama_transfer." ( ".$data_db->nama_bank_transfer." ) "; ?></td>
                  <td ><?= $data_db->no_rekening." - ".$data_db->nama." ( ".$data_db->nama_bank." ) "; ?></td>
                  <td ><?php echo $data_db->jumlah_transfer; ?></td>
                  <td >
                    <a href ="<?= base_url().'asset/Picture/Bukti_Transfer/'.$data_db->bukti_transfer; ?>" target="_blank" >
                        <img src="<?= base_url().'asset/Picture/Bukti_Transfer/'.$data_db->bukti_transfer; ?>" style="width:150px;height: 100px;" alt="no-image">
                      </a>
                  </td>
                  <td>

                    <?php if($data_db->status_konfirmasi == 0) { ?>
                      <a href="<?php echo site_url($on_menu.'/konfirmasi/'.$data_db->id_transfer);?>" id="<?php echo $data_db->id_transfer ?>" class="konfirmasi btn btn-warning fa fa-confirm"> Konfirmasi</a>
                      <a href="<?php echo site_url($on_menu.'/delete_data/'.$data_db->id_transfer);?>" id="<?php echo $data_db->id_transfer ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                    <?php }else { ?>
                      <label class="label " style="background-color: green;color: white;padding: 10px;">Terkonfirmasi</label>
                    <?php } ?>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>Id transfer</label>
              <input type="text" class="form-control" name="id_transfer" placeholder="Id transfer" required maxlength="5" readonly value="<?= $auto_id; ?>" />
            </div>
            <div class="form-group">
              <label>Nama transfer</label>
              <input type="text" class="form-control" name="nama_transfer" placeholder="Nama transfer" required />
            </div>
            <div class="form-group">
              <label>Tipe</label>
              <input type="text" class="form-control" name="tipe" placeholder="Tipe" required />
            </div>
            <div class="form-group">
              <label>Keterangan</label>
              <input type="text" class="form-control" name="keterangan" placeholder="Keterangan" required />
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      
      $('.konfirmasi').click(function(e){
          e.preventDefault();
          var link = $(this).attr('href');

          swal({
              title: "Are you sure?",
              text: "By clicking 'OK' you data will be confirm.",
              type: "warning",
              showCancelButton: true
          },
          function(){
              window.location.href = link;
          });
      });


   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
