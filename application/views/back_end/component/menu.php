<header class="main-header">
    <!-- Logo -->
    <a href="<?php echo site_url('C_front_end') ?>" class="logo">
      <!-- mini logo for sidebar mini 50x50 pixels -->
      <span class="logo-mini"><img src="<?php echo base_url()."asset/logo1.jpg" ?>" style="width: 50px;height: 50px;"></span>
      <!-- logo for regular state and mobile devices -->
      <span class="logo-lg"><b>SMAN 5 TAMSEL</b></span>
    </a>
    <!-- Header Navbar: style can be found in header.less -->
    <nav class="navbar navbar-static-top">
      <!-- Sidebar toggle button-->
      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </a>

      <div class="navbar-custom-menu">
        <ul class="nav navbar-nav">
          
        <?php if ($this->session->userdata('errorProfile')) {?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error</h4>
                <?php echo $this->session->userdata('errorProfile'); ?>
              </div>
        <?php } else if ($this->session->userdata('prosesProfile')=='update') {?>
            
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Success</h4>
                Data Success in Update
              </div>
        <?php }$this->session->unset_userdata('prosesProfile');$this->session->unset_userdata('errorProfile');?>
        

          <!-- User Account: style can be found in dropdown.less -->
          <li class="dropdown user user-menu">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
              
              <img src="<?php echo base_url() ?>asset/Picture/us.png" class="user-image" alt="" >
             
              <span class="hidden-xs"><i class="fa fa-circle text-success"></i> <?php echo $this->session->userdata('nama'); ?></span>
            </a>
            <ul class="dropdown-menu">
              <!-- User image -->
              <li class="user-header">
              
                <img src="<?php echo base_url() ?>asset/Picture/us.png" class="img-circle" alt="" >
              
                <p>
                  <?php echo $this->session->userdata('nama'); ?>
                  
                </p>
                <h4 style="color:white;"><?php echo $this->session->userdata('status');  ?></h4>
              </li>
              <!-- Menu Footer-->
              <li class="user-footer">
                <div class="pull-left">
                  <?php if($this->session->userdata('status') == "Guru") { ?>
                  <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#Profile" id="edit_guru">
                    Profile
                  </button>
                  <?php } else if($this->session->userdata('status') == "Siswa"){?>
                    <button type="button" class="btn btn-default btn-flat" data-toggle="modal" data-target="#Profile" id="edit_siswa">
                      Profile
                    </button>
                  <?php } ?>

                </div>
                <div class="pull-right">
                  <a href="<?php echo site_url('LoginController/logout'); ?>" class="btn btn-default btn-flat">Log Out</a>
                </div>
              </li>
            </ul>
          </li>
          <!-- Control Sidebar Toggle Button -->
          <li>
            <a href="#" data-toggle="control-sidebar"><i class="fa fa-cog"></i></a>
          </li>
        </ul>
      </div>
    </nav>
  </header>

  <!-- =============================================== -->

  <!-- Left side column. contains the sidebar -->
  <aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
      <!-- Sidebar user panel -->
      <div class="user-panel" >
        <div class="pull-left image">
          <img src="<?php echo base_url() ?>asset/Picture/us.png" class="img-circle" alt="User Image" style="width: 50px;height: 50px;">
        </div>
        <div class="info">

          <p><?php echo $this->session->userdata('nama'); ?></p>
          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
          
        </div>

      <br><br>
      </div>
      <!-- search form -->
      <form action="<?php echo site_url('C_dashboard/search')?>" method="POST" class="sidebar-form" target="_blank">
        <div class="input-group">
          <input type="text" nama="searchGoogle" class="form-control" placeholder="Search on Google..">
              <span class="input-group-btn">
                <button type="submit" nama="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i>
                </button>
              </span>
        </div>
      </form>
      <!-- /.search form -->
      <!-- sidebar menu: : style can be found in sidebar.less -->
      <ul class="sidebar-menu">
        <li class="header">MAIN NAVIGATION</li>
          <li <?php if($title == "dashboard"){ echo 'class="active"'; } ?> >
            <a href="<?php echo site_url('C_dashboard') ?>"><i class="fa fa-dashboard"></i> <span>Dashboard</span></a>
          </li>

        <li class="treeview menu <?php if($title == "Muzakki" || $title == "Mustahiq" || $title == "Kategori Muzakki" || $title == "Kategori Mustahiq"){ echo 'active'; } ?>">
          <a href="#" id="master">
            <i class="fa fa-bars"></i> <span>Master</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="menu <?php if($title == "Muzakki"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/muzakki'); ?>">
                <i class="fa fa-fw fa-users"></i>
                  Muzakki
                </a>  
              </li>
              <li class="menu <?php if($title == "Mustahiq"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/mustahiq'); ?>">
                <i class="fa fa-fw fa-users"></i>
                  Mustahiq
                </a>
              </li>
              <li class="menu <?php if($title == "Kategori Muzakki"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/kat_muzakki'); ?>">
                <i class="fa fa-fw fa-tags"></i>
                  Kategori Muzakki
                </a>
              </li>
              <li class="menu <?php if($title == "Kategori Mustahiq"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/kat_mustahiq'); ?>">
                <i class="fa fa-fw fa-tags"></i>
                  Kategori Mustahiq
                </a>
              </li>
              <li class="menu <?php if($title == "Program ZIS"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/program_zis'); ?>">
                <i class="fa fa-fw fa-tags"></i>
                  Program ZIS
                </a>
              </li>
              <li class="menu <?php if($title == "Rekening"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/rekening'); ?>">
                <i class="fa fa-fw fa-tags"></i>
                  Rekening
                </a>
              </li>
              <li class="menu <?php if($title == "Saldo"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/saldo'); ?>">
                <i class="fa fa-fw fa-tags"></i>
                  Saldo
                </a>
              </li>

              <!-- Course -->
              
          </ul>
        </li>
        <li class="treeview menu <?php if($title == "Berita" || $title == "Galeri"){ echo 'active'; } ?>">
          <a href="#" id="master">
            <i class="fa fa-bars"></i> <span>Post</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="menu <?php if($title == "Berita"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/post_berita'); ?>">
                <i class="fa fa-fw fa-news"></i>
                  Berita
                </a>  
              </li>
              <li class="menu <?php if($title == "Galeri"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/post_galeri'); ?>">
                <i class="fa fa-fw fa-images"></i>
                  Galeri
                </a>
              </li>

              <!-- Course -->
              
          </ul>
        </li>
        <li class="treeview menu <?php if($title == "Konfirmasi Transfer" || $title == "Penerimaan ZIS" || $title == "Penyaluran ZIS"){ echo 'active'; } ?>">
          <a href="#" id="master">
            <i class="fa fa-book"></i> <span>Proses</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
              <li class="menu <?php if($title == "Konfirmasi Transfer"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/Konfirmasi_transfer'); ?>">
                <i class="fa fa-fw fa-book"></i>
                  Konfirmasi Transfer
                </a>  
              </li>
              <li class="menu <?php if($title == "Penerimaan ZIS"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/penerimaan_zis'); ?>">
                <i class="fa fa-fw fa-book"></i>
                  Penerimaan ZIS
                </a>
              </li>
              <li class="menu <?php if($title == "Penyaluran ZIS"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('BackendController/penyaluran_zis'); ?>">
                <i class="fa fa-fw fa-book"></i>
                  Penyaluran ZIS
                </a>
              </li>

              <!-- Course -->
              
          </ul>
        </li>
        <!-- Course -->

        <!-- Laporan -->
        <li class="treeview m <?php if($title == "Laporan Penerimaan ZIS" || $title == "Laporan Penyaluran ZIS" || $title == "Laporan Saldo"){ echo 'active'; } ?>">
          <a href="#">
            <i class="fa fa-file"></i> <span>Laporan</span>
            <span class="pull-right-container">
              <i class="fa fa-angle-left pull-right"></i>
            </span>
          </a>
          <ul class="treeview-menu">
             
              <li class="menu <?php if($title == "Laporan Penerimaan ZIS"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('C_laporan_master'); ?>">
                <i class="fa fa-fw fa-file"></i>
                  Laporan Penerimaan ZIS
                </a>
              </li>

              <li class="menu <?php if($title == "Laporan Penyaluran ZIS"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('C_laporan_master'); ?>">
                <i class="fa fa-fw fa-file"></i>
                  Laporan Penyaluran ZIS
                </a>
              </li>

              <li class="menu <?php if($title == "Laporan Saldo"){ echo 'active'; } ?>">
                <a href="<?php echo site_url('C_laporan_master'); ?>">
                <i class="fa fa-fw fa-file"></i>
                  Laporan Saldo
                </a>
              </li>

          </ul>
        </li>
                
      </ul>
    </section>
    <!-- /.sidebar -->
  </aside>

    