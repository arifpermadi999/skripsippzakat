<link rel="shortcut icon" href="<?php echo base_url();?>asset/logoweb.jpg"/>
    
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.6 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/bootstrap/css/bootstrap.min.css">
  <!-- Select2 -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/plugins/select2/select2.min.css">
  <!-- Font Awesome -->
  <!-- link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  --><!-- Ionicons -->
  <!--<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/ionicons/2.0.1/css/ionicons.min.css">
  -->
  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css">
  
  <!-- Theme style -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
       folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/dist/css/skins/_all-skins.min.css">

 <!-- DataTables -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/plugins/datatables/dataTables.bootstrap.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  
  <!-- bootstrap datepicker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/plugins/datepicker/datepicker3.css">

  <!-- iCheck for checkboxes and radio inputs -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/plugins/iCheck/all.css">

  <!-- Bootstrap time Picker -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/BackEnd/plugins/timepicker/bootstrap-timepicker.min.css">

  <!-- Sweet Alert -->
  <link rel="stylesheet" href="<?php echo base_url() ?>asset/SweetAlert/sweetalert.css">