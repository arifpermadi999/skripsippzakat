
<!-- Sweet Alert -->
<script src="<?php echo base_url() ?>asset/SweetAlert/sweetalert.min.js"></script>

<!-- jQuery 2.2.3 -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/jQuery/jquery-2.2.3.min.js"></script>
<!-- Bootstrap 3.3.6 -->
<script src="<?php echo base_url() ?>asset/BackEnd/bootstrap/js/bootstrap.min.js"></script>
<!-- DataTables -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/datatables/jquery.dataTables.min.js"></script>

<script src="<?php echo base_url() ?>asset/BackEnd/plugins/datatables/dataTables.bootstrap.min.js"></script>
<!-- SlimScroll -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/slimScroll/jquery.slimscroll.min.js"></script>
<!-- FastClick -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/fastclick/fastclick.js"></script>
<!-- AdminLTE App -->
<script src="<?php echo base_url() ?>asset/BackEnd/dist/js/app.min.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="<?php echo base_url() ?>asset/BackEnd/dist/js/demo.js"></script>


<script type="text/javascript" src="<?php echo base_url() ?>asset/Greensock/SplitText.min.js"></script>

<script src="<?php echo base_url() ?>asset/Greensock/TweenMax.min.js"></script>

<!-- Bootstrap WYSIHTML5 -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>

<!-- InputMask -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/input-mask/jquery.inputmask.js"></script>
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/input-mask/jquery.inputmask.date.extensions.js"></script>
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/input-mask/jquery.inputmask.extensions.js"></script>


<!-- Select2 -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/select2/select2.full.min.js"></script>

  <!-- script buat sendiri -->
  <script type="text/javascript">
  	$(document).ready(function(){ 
      
      $('.number-only').keyup(function(e) {
          if(this.value!='-')
            while(isNaN(this.value))
              this.value = this.value.split('').reverse().join('').replace(/[\D]/i,'')
                                     .split('').reverse().join('');
      });

      $('.delete').click(function(e){
          e.preventDefault();
          var link = $(this).attr('href');

          swal({
              title: "Are you sure?",
              text: "By clicking 'OK' you data will be deleted.",
              type: "warning",
              showCancelButton: true
          },
          function(){
              window.location.href = link;
          });
      });

    
      
            //bootstrap WYSIHTML5 - text editor
      $(".textarea").wysihtml5();
      
      //Timepicker
      $(".timepicker").timepicker({
        showInputs: false,
        showMeridian: false 
      });
          
      //Initialize Select2 Elements
      $(".select2").select2();

      
      var check="1";
      //checkbox buat liat pasword
     $("#check_password").change(function() {
      if(check){
        $("#pass").attr('type', 'text');
        check=""; 
      }
      else{
        $("#pass").attr('type', 'password');
        check="1";  
      }
      })
  	})
  </script>
<!-- bootstrap datepicker -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/datepicker/bootstrap-datepicker.js"></script>

<!-- bootstrap time picker -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/timepicker/bootstrap-timepicker.min.js"></script>
  <!-- script dari admin lte -->
  
  <script>
    

    $(function () {
      $("#example1").DataTable();
      $("#example2").DataTable();
      
      $('#example3').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false,
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>

