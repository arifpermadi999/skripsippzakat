<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?= $on_menu; ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>
                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>No Rekening</th>
                <th>Bank</th>
                <th>Nama</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td id="Id<?= $data_db->id_t_rekening; ?>"><?php echo $data_db->id_t_rekening; ?></td>
                  <td id="no_rekening<?= $data_db->id_t_rekening; ?>"><?php echo $data_db->no_rekening; ?></td>
                  <td id="no_rekening<?= $data_db->id_t_rekening; ?>"><?php echo $data_db->nama_bank; ?></td>
                  <td id="no_rekening<?= $data_db->id_t_rekening; ?>"><?php echo $data_db->nama; ?></td>
                  <td>
                    <a href="#" id="<?php echo $data_db->id_t_rekening ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url($on_menu.'/delete_data/'.$data_db->id_t_rekening);?>" id="<?php echo $data_db->id_t_rekening ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>Id Rekening</label>
              <input type="text" class="form-control" name="id_t_rekening" placeholder="Id Rekening" required maxlength="5" value="<?= $auto_id; ?>" readonly/>
            </div>
            <div class="form-group">
              <label>No Rekening</label>
              <input type="text" class="form-control" name="no_rekening" placeholder="No Rekening" required />
            </div>
            <div class="form-group">
              <label>Bank</label>
              <input type="text" class="form-control" name="nama_bank" placeholder="No Rekening" required />
            </div>
            <div class="form-group">
              <label>Nama</label>
              <input type="text" class="form-control" name="nama" placeholder="Nama" required />
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("input[name='id_t_rekening']").val("<?= $auto_id; ?>");
        $("#submit").val("Simpan");
      });
      $(".edit").click(function(){
        $("#submit").val("Simpan");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/update_data')?>");
        var id = $(this).attr("id");
        $("input[name='id_t_rekening']").val( $("#Id" + id + "").text() );
        $("input[name='nama_bank']").val( $("#nama_bank" + id + "").text() );
        $("input[name='no_rekening']").val( $("#no_rekening" + id + "").text() );
        $("input[name='nama']").val( $("#nama" + id + "").text() );
 

      });

   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
