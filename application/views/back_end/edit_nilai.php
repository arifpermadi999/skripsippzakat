<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>
        <table>
          <tr>
            <td><h2> NIS </h2></td>
            <td><h2> : </h2></td>
            <td><h2> <?php echo $nis; ?> </h2></td>
          </tr>
          <tr>
            <td><h2> Nama </h2></td>
            <td><h2> : </h2></td>
            <td><h2> <?php echo $nama; ?> </h2></td>
          </tr>
          <tr>
            <td><h2> Kelas </h2></td>
            <td><h2> : </h2></td>
            <td><h2> <?php echo $kelas; ?> </h2></td>
          </tr>
        </table>
        <br><br>
        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
        <br><br>
        <ul class="nav nav-tabs">
          <li class="active"><a data-toggle="tab" href="#nilai">Nilai</a></li>
          <li><a data-toggle="tab" href="#nilai_lama">Nilai Lama</a></li>
        </ul>

        <div class="tab-content">
          <div id="nilai" class="tab-pane fade in active">

            <div class="form-group">
              <label>Semester</label>
              <select id="semester" class="form-control" style="width:200px;">
                <option value=""> -- Select Semester -- </option>
                <option value="1">1</option>
                <option value="2">2</option>
              </select>
            </div>

            <h3>Nilai</h3><br>
            <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>Mata Pelajaran</th>
                <th>Nilai UTS</th>
                <th>Nilai Uas</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <?php foreach ($data as $data_db) { ?>
                <tr class="<?php echo $data_db->semester; ?> semester">
                  <?php $data_mapel_db = $this->db->where('id_mapel',$data_db->id_mapel)->get('tbl_mapel')->row_object(); ?>
                  <td>
                    <?php echo $data_mapel_db->mapel; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nilai_uts; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nilai_uas; ?>
                  </td>
                  <td>
                    <a href="#" id="<?php echo $data_db->id_nilai ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url('C_nilai/delete_data/'.$data_db->id_nilai."/".$data_db->id_kelas."/".$data_db->nis);?>" id="<?php echo $data_db->nis ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
          </div>
          <div id="nilai_lama" class="tab-pane fade">
            <div class="form-group">
              <label>Kelas</label>
              <select id="kelaslama" class="form-control" style="width:200px;">
                <option value=""> -- Select Kelas -- </option>
                <?php foreach($data_kelas_lama as $data_kelas_lama){ ?>
                    <?php if($kelas == $data_kelas_lama->id_kelas){?>
                    <option value="<?php echo $data_kelas_lama->id_kelas; ?>"><?php echo $data_kelas_lama->kelas; ?></option>
                <?php } 
                } ?>
              </select>
            </div>
           <div class="form-group">
              <label>Semester</label>
              <select id="semesterlama" class="form-control" style="width:200px;">
                <option value=""> -- Select Semester -- </option>
                <option value="1">1</option>
                <option value="2">2</option>
              </select>
            </div>

            <h3>Nilai Lama</h3><br>
            <table class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Mata Pelajaran</th>
                <th>Nilai UTS</th>
                <th>Nilai Uas</th>
              </tr>
            </thead>
            <tbody id="nilailama">
            
            </tbody>
          </table>
          </div>
        </div>
                 <!-- Main content -->
          
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">

            <input type="hidden" name="id_nilai" value="">
            <input type="hidden" name="id_kelas" value="<?php echo $id_kelas; ?>">
            <input type="hidden" name="nis" value="<?php echo $nis; ?>">            
            <div class="form-group">
              <label>Mata Pelajaran</label>
              <select class="form-control" name="id_mapel">
                <?php foreach($data_mapel as $mapel){ ?>
                <option value="<?php echo $mapel->id_mapel; ?>"><?php echo $mapel->mapel; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label>Semester</label>
              <select name="semester" class="form-control">
                <option value="1">1</option>
                <option value="2">2</option>
              </select>
            </div>

            <div class="form-group">
              <label>Nilai UTS</label>
              <input type="text" class="form-control number-only" name="nilai_uts" placeholder="UTS" required maxlength="3"/>
            </div>     
            <div class="form-group">
              <label>Nilai UAS</label>
              <input type="text" class="form-control number-only" name="nilai_uas" placeholder="UAS" required maxlength="3"/>
            </div>     

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){

      var id_kelas = "<?php echo $id_kelas; ?>"
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url('C_nilai/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input[name='id_nilai']").val("");
        $("input[name='nilai_uts']").val("");
        $("input[name='nilai_uas']").val("");
        $("#submit").val("Simpan");
      });

      $(".semester").hide();
      $("#semester").change(function(){
        $(".semester").hide();
        var id_semester = $(this).val();
        $("." + id_semester).show();
      })
      $("#semesterlama").change(function(){
        var semester = $("#semesterlama").val();
        var kelas = $("#kelaslama").val()
        if(semester && kelas){

          $.ajax({
            url:"<?php echo site_url('C_nilai/getDataNilai')?>" + "/" + semester  + "/" + kelas + "/" + id_kelas,
            dataType:"html",
            type:"get",
            success:function(response){
                $("#nilailama").html(response);
            }
          }) 
        }
      })

      $("#kelaslama").change(function(){
        var semester = $("#semesterlama").val();
        var kelas = $("#kelaslama").val()
        if(semester && kelas){
         
          $.ajax({
            url:"<?php echo site_url('C_nilai/getDataNilai')?>" + "/" + semester  + "/" + kelas + "/" + id_kelas,
            dataType:"html",
            type:"get",
            success:function(response){
                $("#nilailama").html(response);
            }
          }) 
        }
      })
      $(".edit").click(function(){
        $("#submit").val("Update");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url('C_nilai/update_data')?>");
        var id = $(this).attr("id");

        $.ajax({
          url : "<?php echo site_url('C_nilai/getDataId')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){
            $("input[name='id_nilai']").val(response.id_nilai);
            $("input[name='nilai_uts']").val(response.nilai_uts);
            $("input[name='nilai_uas']").val(response.nilai_uas);

            $('select[name="semester"] option[value='+ response.semester +']').attr('selected','selected');
            $('select[name="id_mapel"] option[value='+ response.id_mapel +']').attr('selected','selected');
          }
        })
      })
    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
