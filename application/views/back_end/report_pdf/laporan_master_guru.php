<!DOCTYPE html>
<html>
<head>
	<title>Laporan Master Mapel</title>
<style type="text/css">
		table {
    border-collapse: collapse;
    width: 100%;
}

th, td {;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: royalblue;
    color: white;
}
	</style>	
</head>
<body>

	<?php $this->load->view('back_end/report_pdf/header')?>
	<h1 >Laporan Master Guru</h1>
	<br>
  
             <!-- Main content -->
          <table border="1">
            <thead>
              <tr>
                <th>No</th>
                <th>nip</th>
                <th>Nama</th>
                <th>Tempat Tanggal Lahir</th>
                <th>Agama</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Telp</th>
                <th>Jabatan</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nip; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->ttl; ?>
                  </td>
                  <td>
                    <?php echo $data_db->agama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jenis_kelamin; ?>
                  </td>
                  <td>
                    <?php echo $data_db->alamat; ?>
                  </td>
                  <td>
                    <?php echo $data_db->telp; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jabatan; ?>
                  </td>
                  <td>
                    <?php echo $data_db->email; ?>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
</body>
</html>