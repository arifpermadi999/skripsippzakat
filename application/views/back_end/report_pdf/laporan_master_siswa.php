<!DOCTYPE html>
<html>
<head>
	<title>Laporan Master Siswa</title>
<style type="text/css">
		table {
    border-collapse: collapse;
    width: 100%;
}

th, td {;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: royalblue;
    color: white;
}
	</style>	
</head>
<body>

	<?php $this->load->view('back_end/report_pdf/header')?>
	<h1 >Laporan Master Siswa <?php echo $status; ?></h1>
	<br>
  <?php if($kelas){?>
    <h4>
      <?php echo "Kelas : ".$kelas; ?>
    </h4>
  <?php } ?>
	<table border="1">
            <thead>
              <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <?php if(!$kelas){?>
                  <th>Kelas</th>
                <?php } ?>
                <th>Tempat Tanggal Lahir</th>
                <th>Agama</th>
                <th>Jenis Kelamin</th>
                <th>Tahun Ajar</th>
                <th>Status</th>
                <th>Keterangan</th>
                <th>Email</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nis; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nama; ?>
                  </td>
                  <?php if(!$kelas){?>
                    <td>
                      <?php echo $data_db->kelas; ?>
                    </td>
                  <?php } ?>
                  <td>
                    <?php echo $data_db->ttl; ?>
                  </td>
                  <td>
                    <?php echo $data_db->agama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jenis_kelamin; ?>
                  </td>
                  <td>
                    <?php echo $data_db->tahun_ajar; ?>
                  </td>
                  <td>
                    <?php echo $data_db->status; ?>
                  </td>
                  <td>
                    <?php echo $data_db->keterangan; ?>
                  </td>
                  <td>
                    <?php echo $data_db->email; ?>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
</body>
</html>