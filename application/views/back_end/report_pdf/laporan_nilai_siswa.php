<!DOCTYPE html>
<html>
<head>
	<title>Nilai Siswa</title>
	<style type="text/css">
		table {
    border-collapse: collapse;
    width: 70%;
}

th, td {

    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: royalblue;
    color: white;
}
	</style>
</head>
<body>

	<?php $this->load->view('back_end/report_pdf/header')?>
	<h4>Nama : <?php echo $siswa->nama; ?></h4>
	<h4>Kelas : <?php echo $kelas->kelas; ?></h4>
	<h1>Nilai Semester <?php echo $semester; ?></h1>
	<table border="1" >
		<tr>
			<th>No</th>
			<th>Mata Pelajaran</th>
			<th>Nilai UTS</th>
			<th>Nilai UAS</th>
		</tr>
		<?php $no=1;foreach($data as $data_db){?>
		<tr>
			<td style="width: 5%;"><?php echo $no;$no++; ?></td>
			<td style="width: 55%;"><?php echo $data_db->mapel; ?></td>
			<td style="width: 20%;"><?php echo $data_db->nilai_uts;  ?></td>
			<td style="width: 20%;"><?php echo $data_db->nilai_uas; ?></td>
		</tr>
		<?php } ?>
	</table>	
</body>
</html>