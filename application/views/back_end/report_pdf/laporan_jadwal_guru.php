<!DOCTYPE html>
<html>
<head>
	<title>Jadwal Guru</title>
	<style type="text/css">
	.table-hari{
		width: 100%;
	}
	.table-hari td,th{
		padding: 5px;
	}
	</style>
</head>
<body>
	<?php $this->load->view('back_end/report_pdf/header')?>
	<h1 style="text-align: center;">Jadwal Pelajaran</h1>
	<br>
	<table style="width:100%;">
	<?php 
		$data_show = array('Senin','Selasa','Rabu','Kamis',"Jumat",'Sabtu');

		for($i=0;$i<=5;$i++){ ?>
			<?php if($i == 0){?>
				<tr>
			<?php } ?>
				<td style='width:33%;height: 300px;padding: 3px;'>
					<div style="width: 100%;height: 100%;">
					<?php $ada = 0;?>
					<?php 
						foreach ($data as $data_db) {
							$valid_hari = $this->db->query("select j.*,m.mapel,jam.jam,k.kelas from tbl_jadwal j,tbl_mapel m,tbl_jam jam,tbl_kelas k where j.id_kelas = k.id_kelas and j.id_mapel = m.id_mapel and j.id_jam = jam.id_jam and j.id_mapel = '".$data_db->id_mapel."' and j.hari = '".$data_show[$i]."' order by j.hari ASC")->row_object();
							if($valid_hari){
								$ada = 1;
							}
							
					} ?>
					<?php if($ada == 1){?>
					<h2 style="text-align: center;"><?php echo $data_show[$i] ?></h2>
					<table style="border-collapse: collapse;" border="2" class="table-hari">
						<tr>
							<th>Mapel</th>
							<th>Kelas</th>
							<th>Waktu</th>
						</tr>
						<?php 
						foreach ($data as $data_db) {
							$data_guru = $this->db->query("select j.*,m.mapel,jam.jam,k.kelas from tbl_jadwal j,tbl_mapel m,tbl_jam jam,tbl_kelas k where j.id_kelas = k.id_kelas and j.id_mapel = m.id_mapel and j.id_jam = jam.id_jam and j.id_mapel = '".$data_db->id_mapel."' and j.hari = '".$data_show[$i]."' order by j.id_jadwal ASC")->row_object();
							if($data_guru){
								
			
									echo '
										 <tr>
											<td style="width:33%;">'.$data_guru->mapel.'</td>
											<td style="width:33%;">'.$data_guru->kelas.'</td>
											<td style="width:33%;">'.$data_guru->jam.'</td>
										 </tr>';

							}
						} ?>		
					</table>
					<?php } ?>
					</div>
				</td>
			<?php if($i==2){ ?>
				</tr>
				<tr>
			<?php } 
		} ?>
		
		</tr>
	</table>
</body>
</html>