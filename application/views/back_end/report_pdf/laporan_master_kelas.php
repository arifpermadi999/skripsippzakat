<!DOCTYPE html>
<html>
<head>
	<title>Laporan Master Kelas</title>
<style type="text/css">
		table {
    border-collapse: collapse;
    width: 100%;
}

th, td {;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: royalblue;
    color: white;
}
	</style>	
</head>
<body>

	<?php $this->load->view('back_end/report_pdf/header')?>
	<h1 >Laporan Master Kelas</h1>
	<br>
	<table border="1">
            <thead>
              <tr>
                <th>No</th>
                <th>Kelas</th>
                <th>Guru</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td style="width: 30%">
                    <?php echo $data_db->kelas; ?>
                  </td>
                  <td style="width: 20%">
                    <?php echo $data_db->nama; ?>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
</body>
</html>