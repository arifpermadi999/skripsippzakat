<!DOCTYPE html>
<html>
<head>
	<title>Laporan Master Mapel</title>
<style type="text/css">
		table {
    border-collapse: collapse;
    width: 100%;
}

th, td {;
    text-align: left;
    padding: 8px;
}

tr:nth-child(even){background-color: #f2f2f2}

th {
    background-color: royalblue;
    color: white;
}
	</style>	
</head>
<body>

	<?php $this->load->view('back_end/report_pdf/header')?>
	<h1 >Laporan Master Mapel <?php if($guru){ echo "Berdasarkan Guru";} ?></h1>
	<br>
  <?php
    if($guru){ echo "<h4>Guru : ".$guru."</h4>";}  
  ?>
      <table border="1">
            <thead>
              <tr>
                <th>No</th>
                <th>Id Mapel</th>
                <th>Mata Pelajaran</th>
                
                <?php if(!$guru){?>
                  <th>Guru</th>
                <?php } ?>
              
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->id_mapel; ?>
                  </td>
                  <td style="width: 40%;">
                    <?php echo $data_db->mapel; ?>
                  </td>
                  <?php if(!$guru){?>
                  <td>
                    <?php echo $data_db->nama; ?>
                  </td>
                  <?php } ?>
                </tr>
               <?php } ?>
            </tbody>
          </table>
</body>
</html>