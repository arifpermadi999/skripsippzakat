<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
    <style type="text/css">

/*  bhoechie tab */
div.bhoechie-tab-container{
  z-index: 10;
  background-color: #ffffff;
  padding: 0 !important;
  border-radius: 4px;
  -moz-border-radius: 4px;
  border:1px solid #ddd;
  -webkit-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  box-shadow: 0 6px 12px rgba(0,0,0,.175);
  -moz-box-shadow: 0 6px 12px rgba(0,0,0,.175);
  background-clip: padding-box;
  opacity: 0.97;
  filter: alpha(opacity=97);
}
div.bhoechie-tab-menu{
  padding-right: 0;
  padding-left: 0;
  padding-bottom: 0;
}
div.bhoechie-tab-menu div.list-group{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a{
  margin-bottom: 0;
}
div.bhoechie-tab-menu div.list-group>a .glyphicon,
div.bhoechie-tab-menu div.list-group>a .fa {
  color: royalblue;
}
div.bhoechie-tab-menu div.list-group>a:first-child{
  border-top-right-radius: 0;
  -moz-border-top-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a:last-child{
  border-bottom-right-radius: 0;
  -moz-border-bottom-right-radius: 0;
}
div.bhoechie-tab-menu div.list-group>a.active,
div.bhoechie-tab-menu div.list-group>a.active .glyphicon,
div.bhoechie-tab-menu div.list-group>a.active .fa{
  background-color: royalblue;
  background-image: royalblue;
  color: #ffffff;
}
div.bhoechie-tab-menu div.list-group>a.active:after{
  content: '';
  position: absolute;
  left: 100%;
  top: 50%;
  margin-top: -13px;
  border-left: 0;
  border-bottom: 13px solid transparent;
  border-top: 13px solid transparent;
  border-left: 10px solid royalblue;
}

div.bhoechie-tab-content{
  background-color: #ffffff;
  /* border: 1px solid #eeeeee; */
  padding-left: 20px;
  padding-top: 10px;
}

div.bhoechie-tab div.bhoechie-tab-content:not(.active){
  display: none;
}
.color_biru{
  margin-top: 0;color:royalblue !important;
}
</style>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">

            <a href="<?php echo site_url('C_jadwal/cetak_jadwal_siswa/'.$kelas->id_kelas); ?>" class="btn btn-info" target="_blank" id="cetak_jadwal"><i class="fa fa-print"></i> Cetak Jadwal</a><br>
            
            <label style="font-size: 25px;">Kelas</label><label style="font-size: 25px;"> : <?php echo $kelas->kelas; ?></label><br>
            <label>Semester</label>
            <select name="semester_value" id="semester_value" class="form-control" style="width: 50%">
              <option value="1">1</option>
              <option value="2">2</option>
            </select><br>
              <div class="container" style="margin-left: 0px;width: 100%;">
                <div class="row">
                      <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12 bhoechie-tab-container">
                          <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3 bhoechie-tab-menu">
                          <?php 
                            $data_show = array('Senin','Selasa','Rabu','Kamis',"Jumat",'Sabtu');
                          ?>
                            <div class="list-group">
                              <?php for($i=0;$i<=5;$i++){ ?>
                                <a href="#" class="list-group-item <?php if($i==0){ echo "active";} ?> text-center" data-info="<?php echo $data_show[$i]; ?>">
                                  <h4 class="glyphicon glyphicon-time"></h4><br/><?php echo $data_show[$i]; ?>
                                </a>
                              <?php }  ?>
                            </div>
                          </div>
                          <div class="col-lg-9 col-md-9 col-sm-9 col-xs-9 bhoechie-tab">

                              <?php for($i=0;$i<=5;$i++){ ?>
                              <!-- flight section -->
                              <div class="bhoechie-tab-content <?php if($i==0){ echo "active";} ?>">
                                  
                                  <center>
                                    <h1 class="glyphicon glyphicon-time" style="font-size:8em;color:royalblue"></h1>
                                    <h2 class="color_biru">Jadwal <?php echo $data_show[$i]; ?></h2>
                                    
                                  </center>
                                  <!-- content senin -->
                                  <div class="content-jadwal" id="content-<?php echo $data_show[$i]; ?>">  
                                    <table class="table table-bordered">
                                      <thead>
                                        <tr>
                                          <th>No</th>
                                          <th>Jadwal</th>
                                          <th>Waktu</th>
                                        </tr>
                                      </thead>
                                      <tbody id="body-<?php echo $data_show[$i]; ?>">

                                      </tbody>
                                    </table>
                                  </div>
                                  <!-- ======== -->
                              </div>

                              <?php } ?>
                          </div>
                      </div>
                </div>
              </div>
           
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>

  <script type="text/javascript">
      var id_kelas = "<?php echo $kelas->id_kelas;?>";
      var semester_first = "1";
      var hari_value = "Senin";
      
      
    $(document).ready(function() {

      /*saat page di buka load data di hari senin dan semester 1*/
      $.ajax({
          url: "<?php echo site_url('C_jadwal/show_jadwal_siswa'); ?>" + "/" + id_kelas + "/" + hari_value + "/" + semester_first,
          type:"get",
          dataType:"html",
          cache : true,
          success : function(response){                
                $("#body-Senin").html(response);
                $("#btndata_Senin").hide();
                $("#content-Senin").css("display","block");
          },
          error: function (request, status, error) {
              alert(request.responseText);
          }
      })
      $("select[name='semester_value']").change(function(){
        console.log($(this).val() + " hari " + hari_value);
        $.ajax({
          url: "<?php echo site_url('C_jadwal/show_jadwal_siswa'); ?>" + "/" + id_kelas + "/" + hari_value + "/" + $(this).val(),
          type:"get",
          dataType:"html",
          cache : true,
          success : function(response){                
                $("#body-"  + hari_value).html(response);
                $("#btndata_" + hari_value).hide();
                $("#content-" + hari_value).css("display","block");
          },
          error: function (request, status, error) {
              alert(request.responseText);
          }
        })
      })
      /*klik menu hari*/
      $(".list-group-item").click(function(){
        var hari = $(this).attr("data-info");
        var semester = $("#semester_value").val();
        hari_value = hari;
        $("input[name='hari']").val(hari);
        $.ajax({
          url: "<?php echo site_url('C_jadwal/show_jadwal_siswa'); ?>" + "/" + id_kelas + "/" + hari + "/" + semester,
          type:"get",
          dataType:"html",
          cache : true,
          success : function(response){                
                $("#body-" + hari).html(response);
                $("#btndata_" + hari).hide();
                $("#content-" + hari).css("display","block");
          },
          error: function (request, status, error) {
              alert(request.responseText);
          }
        })          
      })
      /*js buat click menu(udh dari sononya)*/
      $("div.bhoechie-tab-menu>div.list-group>a").click(function(e) {
          e.preventDefault();
          $(this).siblings('a.active').removeClass("active");
          $(this).addClass("active");
          var index = $(this).index();
          $("div.bhoechie-tab>div.bhoechie-tab-content").removeClass("active");
          $("div.bhoechie-tab>div.bhoechie-tab-content").eq(index).addClass("active");
      });
      $("#cetak_jadwal").click(function(e){
        e.preventDefault();
        var semester = $("#semester_value").val();
        var link = $(this).attr("href") + "/" + semester + "/Jadwal_Siswa";
        window.location.href = link;
      })

    });
    
    /**/
  </script>
  
  <?php $this->load->view($modal_profile); ?>
</body>
</html>
