<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>

                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>nip</th>
                <th>Nama</th>
                <th>Tempat Tanggal Lahir</th>
                <th>Agama</th>
                <th>Jenis Kelamin</th>
                <th>Alamat</th>
                <th>Telp</th>
                <th>Jabatan</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nip; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->ttl; ?>
                  </td>
                  <td>
                    <?php echo $data_db->agama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jenis_kelamin; ?>
                  </td>
                  <td>
                    <?php echo $data_db->alamat; ?>
                  </td>
                  <td>
                    <?php echo $data_db->telp; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jabatan; ?>
                  </td>
                  <td>
                    <?php echo $data_db->email; ?>
                  </td>
                  <td>
                    <a href="#" id="<?php echo $data_db->nip ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url('C_guru/delete_data/'.$data_db->nip);?>" id="<?php echo $data_db->nip ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>NIP</label>
              <input type="text" class="form-control number-only" name="nip" placeholder="NIP" required maxlength="10"/>
            </div>     
            <div class="form-group">
              <label>Nama</label>

              <input type="text" class="form-control" name="nama" placeholder="Nama" required maxlength="50"/>
                
            </div>

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" placeholder="Email" required maxlength="30"/>
            </div>
            <div class="form-group">
              <label>Password</label>
              <div class="input-group">
                  <input type="password" class="form-control" name="password" placeholder="Password" required maxlength="15" id="pass" />
                    <div class="input-group-addon">
                      <label>Look &nbsp;&nbsp;</label> 
                      <input type="checkbox" name="check" id="check_password">
                    </div>
              </div>
            </div>
            <div class="form-group">
              <label>jenis Kelamin</label>
              <select name="jenis_kelamin" id="GenderSelect" class="form-control">
                <option value="Pria">Pria</option>
                <option value="Perempuan">Perempuan</option>
                
              </select>
            </div>
            
            <div class="form-group">
              <label>Tempat Tanggal Lahir</label>
                <input type="text" class="form-control pull-right" name="ttl" placeholder="TTL" required maxlength="50">

            </div><br>

            <div class="form-group">
              <label>Agama</label>
              <select name="agama" class="form-control">
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
              </select>
                
            </div>

            <div class="form-group">
              <label>Jabatan</label>
              <select name="jabatan" class="form-control">
                <option value="Guru">Guru</option>
                <option value="Guru TK">Guru TK</option>

            <div class="form-group">
              <label>alamat</label>
              <textarea class="form-control" name="alamat" placeholder="alamat" maxlength="100"></textarea>
            </div>
            <div class="form-group">
              <label>Telp</label>
              <input type="text" class="form-control number-only" name="telp" placeholder="Telp" required maxlength="12"/>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" name="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url('C_guru/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("#submit").val("Simpan");
      });

      $("input[name='nip']").on("input",function(){

        var id =$(this).val(); 
        if(!id){
          id = "ngasal";
        }
        console.log("<?php echo site_url('C_guru/validasi_id')?>" + "/" + id);
        $.ajax({
          url : "<?php echo site_url('C_guru/validasi_id')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){

            console.log(response.validasi);
            if(response.validasi == "sama"){
              $("input[name='nip']").val("");
              swal({
                  title: "Validasi",
                  text: "Id Mapel Sudah Ada",
                  type :"warning"
              });
              $("input[name='nip']").focus();
            }

          }
        })

      })
      $(".edit").click(function(){
        $("#submit").val("Update");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url('C_guru/update_data')?>");
        var id = $(this).attr("id");
        $.ajax({
          url : "<?php echo site_url('C_guru/getDataId')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){
            $("input[name='nip']").val(response.nip);
            $("input[name='nama']").val(response.nama);
            $("input[name='email']").val(response.email);
            $("input[name='password']").val(response.password);
            $("input[name='ttl']").val(response.ttl);
            $("textarea[name='alamat']").val(response.alamat);
            $("input[name='telp']").val(response.telp);

            $('select[name="jabatan"] option[value='+ response.jabatan +']').attr('selected','selected');
            $('select[name="jenis_kelamin"] option[value='+ response.jenis_kelamin +']').attr('selected','selected');
            $('select[name="agama"] option[value='+ response.agama +']').attr('selected','selected');
          }
        })

      });

   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
