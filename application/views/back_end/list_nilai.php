<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <label>Kelas</label>
        <select class="form-control" name="id_kelas" style="width: 200px;">
                <option value=""> -- Select Kelas -- </option>
                <?php foreach($kelas as $kelas){ ?>
                <option value="<?php echo $kelas->id_kelas; ?>"><?php echo $kelas->kelas; ?></option>
                <?php } ?>
        </select>
          <br><br>

                 <!-- Main content -->
          <div id="table">
          </div>
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama Siswa</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
            <?php $no=1;foreach($siswa as $data_db) { ?>
                <tr class="<?php echo $data_db->id_kelas; ?> siswa">
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nis; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nama; ?>
                  </td>
                  <td>
                    <a href="<?php echo site_url('C_nilai/edit_nilai/'.$data_db->id_kelas."/".$data_db->nis) ?>" class='btn btn-info fa fa-edit'> Edit Nilai</a>
                  </td>
                </tr>
              <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $(".siswa").hide();
      $("select[name='id_kelas']").change(function(){
        var id = $(this).val();
        if(id==""){
          $(".siswa").hide();
        }else{
          $("." + id).show(); 
        }
      })

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
