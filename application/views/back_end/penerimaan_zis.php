<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title"><?php echo $title; ?></h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>
                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Nama Muzakki</th>
                <th>Kategori ZIS</th>
                <th>No Transfer</th>
                <th>Saldo</th>
                <th>Jumlah Terima</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>

              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td id="Id<?= $data_db->id_penerimaan_zis; ?>"><?php echo $data_db->nama_muzakki; ?></td>
                  <td id="nama_muzakki<?= $data_db->id_penerimaan_zis; ?>"><?php echo $data_db->nama_program; ?></td>
                  <td id="id_transfer<?= $data_db->id_penerimaan_zis; ?>"><?php echo $data_db->id_transfer; ?></td>
                  <td id="nama_saldo<?= $data_db->id_penerimaan_zis; ?>"><?php echo $data_db->nama_saldo; ?></td>
                  <td id="jumlah_terima<?= $data_db->id_penerimaan_zis; ?>"><?php echo $data_db->jumlah_terima; ?></td>
                  <td>
                    
                    <a href="<?php echo site_url($on_menu.'/delete_data/'.$data_db->id_penerimaan_zis);?>" id="<?php echo $data_db->id_saldo ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>No Penerimaan</label>
              <input type="text" class="form-control" name="id_penerimaan_zis" placeholder="No Penerimaan" required maxlength="12" readonly value="<?= $auto_id; ?>" />
            </div>

            <div class="form-group">
              <label>Tanggal Penerimaan</label>
              <div class="input-group date datepicker" >
                <input type="text" class="form-control" name="tgl_penerimaan_zis" placeholder="Tanggal Penerimaan" required data-date-format="yyyy-mm-dd"/>
                <span class="input-group-addon">
                    <span class="glyphicon glyphicon-calendar"></span>
                </span>
              </div>
            </div>
            <div class="form-group">
              <label>Nama Muzakki</label>
              <select class="form-control" name="id_muzakki" required id="muzakki">
                <option value="">-- Pilih Muzakki --</option>
                <?php foreach($data_muzakki as $data_db){ ?>
                <option value="<?php echo $data_db->id_muzakki; ?>"><?php echo $data_db->nama_muzakki; ?></option>
                <?php } ?>
              </select>
            </div>
            <div class="form-group">
              <label>Via</label>
              <select class="form-control" name="via">
                <option value="Transfer">Transfer</option>
                <option value="Tunai">Tunai</option>
              </select>
            </div>
            <div class="form-group">
              <label>No Transfer</label>
              <select class="form-control" name="no_transfer2">
                
              </select>
            </div>

            <input type="hidden" class="form-control" name="id_transfer" value=""  />
            
            <div class="form-group">
              <label>No Rekening</label>
              <input type="text" class="form-control" name="no_rekening" placeholder="No Rekening" value=""  />
            
            </div>
            <div class="form-group">
              <label>Jenis Saldo</label>
              <select class="form-control" name="id_saldo" required>
                <?php foreach($data_saldo as $data_db){ ?>
                <option value="<?php echo $data_db->id_saldo; ?>"><?php echo $data_db->nama_saldo; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label>Kategori ZIS</label>
              <select class="form-control" name="id_program" required>
                <?php foreach($data_program as $data_db){ ?>
                <option value="<?php echo $data_db->id_program; ?>"><?php echo $data_db->nama_program; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label>Jumlah Terima</label>
              <input type="text" class="form-control" name="jumlah_terima" placeholder="Jumlah Terima" required />
            </div>
            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){

      $('.datepicker').datepicker({
        format: 'yyyy-mm-dd',
      });

      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("input[name='id_penerimaan_zis']").val("<?= $auto_id; ?>");
        $("#submit").val("Simpan");
      });
      $(".edit").click(function(){
        $("#submit").val("Simpan");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/update_data')?>");
        var id = $(this).attr("id");
        $("input[name='saldo']").prop("readonly","true");
        $("input[name='id_saldo']").val( $("#Id" + id + "").text() );
        $("input[name='nama_saldo']").val( $("#nama_saldo" + id + "").text() );
        $("input[name='saldo']").val( $("#saldo" + id + "").text() );

      });
      var data_rekening =[];
      var data_jumlah_terima =[];
      var data_transfer =[];
      $("#muzakki").change(function(){
        var id = $(this).val();
        $.ajax({
          url : "<?php echo site_url('BackendController/getTransferMuzakkiJson')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){
            $("select[name='no_transfer2']").html("<option value=''> -- Pilih No Transfer -- </option>");
            for(var i=0;i<response.length;i++){
              var no_transfer = response[i].id_transfer;
              var no_rekening = response[i].no_rekening_transfer;
              var jumlah_terima = response[i].jumlah_transfer;
              $("select[name='no_transfer2']").append("<option value='" + i + "'>" + no_transfer + "</option>");
              data_rekening[i] = no_rekening;
              data_jumlah_terima[i] = jumlah_terima;
              data_transfer[i] = no_transfer;
            }
          }
        })
      })
      $("select[name='no_transfer2']").change(function(){
        var id = $("select[name='no_transfer2']").val();
    
        var no_rekening = data_rekening[id];
        var jumlah_terima = data_jumlah_terima[id];
        var no_transfer = data_transfer[id];

        $("input[name='no_rekening']").val(no_rekening);
        $("input[name='jumlah_terima']").val(jumlah_terima);
        $("input[name='id_transfer']").val(no_transfer);
        
      })
   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
