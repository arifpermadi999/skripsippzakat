<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>

                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Id</th>
                <th>Nama mustahiq</th>
                <th>Kategori mustahiq</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td id="id_mustahiq<?= $data_db->id_mustahiq; ?>"><?php echo $data_db->id_mustahiq; ?></td>
                  <td id="nama_mustahiq<?= $data_db->id_mustahiq; ?>"><?php echo $data_db->nama_mustahiq; ?></td>
                  <td id="kategori_mustahiq<?= $data_db->id_mustahiq; ?>">
                    <?php echo $data_db->kategori_mustahiq; ?>
                    
                    <label id="id_katmustahiq<?= $data_db->id_mustahiq; ?>" style="display: none;"><?php echo $data_db->id_katmustahiq; ?></label>
                    <label id="alamat<?= $data_db->id_mustahiq; ?>" style="display: none;"><?php echo $data_db->alamat; ?></label>
                    <label id="no_telepon<?= $data_db->id_mustahiq; ?>" style="display: none;"><?php echo $data_db->no_telepon; ?></label>
                  </td>

                  <td>
                    <a href="#" id="<?php echo $data_db->id_mustahiq ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url($on_menu.'/delete_data/'.$data_db->id_mustahiq);?>" id="<?php echo $data_db->id_mustahiq ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>Id mustahiq</label>
              <input type="text" class="form-control" name="id_mustahiq" placeholder="Id mustahiq" required maxlength="5" />
            </div>
            <div class="form-group">
              <label>Nama Mustahiq</label>
              <input type="text" class="form-control" name="nama_mustahiq" placeholder="Nama mustahiq" required />
            </div>

            <div class="form-group">
              <label>Kategori Mustahiq</label>
              <select class="form-control" name="id_katmustahiq">
                <?php foreach($data_katmustahiq as $data_db){ ?>
                <option value="<?php echo $data_db->id_katmustahiq; ?>"><?php echo $data_db->kategori_mustahiq; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label>Alamat</label>
              <input type="text" class="form-control" name="alamat" placeholder="Alamat" required />
            </div>
            <div class="form-group">
              <label>No Telepon</label>
              <input type="text" class="form-control" name="no_telepon" placeholder="No Telepon" required />
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("#submit").val("Simpan");
      });
      $(".edit").click(function(){
        $("#submit").val("Simpan");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/update_data')?>");
        var id = $(this).attr("id");
        
        $("input[name='id_mustahiq']").val( $("#id_mustahiq" + id + "").text() );
        $("input[name='nama_mustahiq']").val( $("#nama_mustahiq" + id + "").text() );
        
        $("input[name='id_mustahiq']").val( $("#id_mustahiq" + id + "").text() );
        
        $('select[name="id_katmustahiq"] option[value=' + $("#id_katmustahiq" + id + "").text() + ']').attr('selected','selected');
        
        $("input[name='alamat']").val( $("#alamat" + id + "").text() );
        $("input[name='no_telepon']").val( $("#no_telepon" + id + "").text() );


      });

   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
