<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
    
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>

                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Id</th>
                <th>Kategori Muzakki</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td id="Id<?= $data_db->id_katmuzakki; ?>"><?php echo $data_db->id_katmuzakki; ?></td>
                  <td id="Kategori<?= $data_db->id_katmuzakki; ?>"><?php echo $data_db->kategori_muzakki; ?></td>
                  <td>
                    <a href="#" id="<?php echo $data_db->id_katmuzakki ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url($on_menu.'/delete_data/'.$data_db->id_katmuzakki);?>" id="<?php echo $data_db->id_katmuzakki ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>Id Muzakki</label>
              <input type="text" class="form-control" name="id_katmuzakki" placeholder="Id Muzakki" required maxlength="5" />
            </div>
            <div class="form-group">
              <label>Kategori Muzakki</label>
              <input type="text" class="form-control" name="kategori_muzakki" placeholder="Kategori Muzakki" required />
            </div>
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("#submit").val("Simpan");
      });
      $(".edit").click(function(){
        $("#submit").val("Simpan");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url($on_menu.'/update_data')?>");
        var id = $(this).attr("id");
         $("input[name='id_katmuzakki']").val( $("#Id" + id + "").text() );
        $("input[name='kategori_muzakki']").val( $("#Kategori" + id + "").text() );
 

      });

   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
