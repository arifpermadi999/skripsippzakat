<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>

                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>NIS</th>
                <th>Nama</th>
                <th>Kelas</th>
                <th>Tempat Tanggal Lahir</th>
                <th>Agama</th>
                <th>Jenis Kelamin</th>
                <th>Tahun Ajar</th>
                <th>Status</th>
                <th>Keterangan</th>
                <th>Email</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nis; ?>
                  </td>
                  <td>
                    <?php echo $data_db->nama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->kelas; ?>
                  </td>
                  <td>
                    <?php echo $data_db->ttl; ?>
                  </td>
                  <td>
                    <?php echo $data_db->agama; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jenis_kelamin; ?>
                  </td>
                  <td>
                    <?php echo $data_db->tahun_ajar; ?>
                  </td>
                  <td>
                    <?php echo $data_db->status; ?>
                  </td>
                  <td>
                    <?php echo $data_db->keterangan; ?>
                  </td>
                  <td>
                    <?php echo $data_db->email; ?>
                  </td>
                  <td>
                    <a href="#" id="<?php echo $data_db->nis ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url('C_siswa/delete_data/'.$data_db->nis);?>" id="<?php echo $data_db->nis ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>

                    <?php if($data_db->status == "Aktif"){ ?>
                      <a href="<?php echo site_url('C_siswa/aktifasi/0/'.$data_db->nis); ?>" class="btn btn-success deaktifasi">Tidak Aktifkan</a>
                    <?php } else if($data_db->status == "Tidak Aktif"){ ?>
                      <a href="<?php echo site_url('C_siswa/aktifasi/1/'.$data_db->nis); ?>" class="btn btn-danger aktifasi">Aktifkan</a>
                    <?php } ?>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">
              <label>NIS</label>
              <input type="text" class="form-control number-only" name="nis" placeholder="NIS" required maxlength="10"/>
            </div>     
            <div class="form-group">
              <label>Nama</label>

              <input type="text" class="form-control" name="nama" placeholder="Nama" required maxlength="50"/>
                
            </div>
            <div class="form-group">
              <label>Kelas</label>
              <select class="form-control" name="id_kelas">
                <?php foreach($data_kelas as $kelas){ ?>
                <option value="<?php echo $kelas->id_kelas; ?>"><?php echo $kelas->kelas; ?></option>
                <?php } ?>
              </select>
            </div>

            <div class="form-group">
              <label>Email</label>
              <input type="email" class="form-control" name="email" placeholder="Email" required maxlength="30"/>
            </div>
            <div class="form-group">
              <label>Password</label>
              <div class="input-group">
                  <input type="password" class="form-control" name="password" placeholder="Password" required maxlength="15" id="pass" />
                    <div class="input-group-addon">
                      <label>Look &nbsp;&nbsp;</label> 
                      <input type="checkbox" name="check" id="check_password">
                    </div>
              </div>
            </div>
            <div class="form-group">
              <label>Jenis Kelamin</label>
              <select name="jenis_kelamin" id="GenderSelect" class="form-control">
                <option value="Pria">Pria</option>
                <option value="Perempuan">Perempuan</option>
                
              </select>
            </div>
            
            <div class="form-group">
              <label>Tempat Tanggal Lahir</label>
                <input type="text" class="form-control pull-right" name="ttl" placeholder="TTL" required maxlength="50">

            </div><br>

            <div class="form-group">
              <label>Agama</label>
              <select name="agama" class="form-control">
                <option value="Islam">Islam</option>
                <option value="Kristen">Kristen</option>
              </select>
                
            </div>

            <div class="form-group">
              <label>Status</label>
              <select name="status" class="form-control">
                <option value="Aktif">Aktif</option>
                <option value="Tidak Aktif">Tidak Aktif</option>
              </select>
                
            </div>
            <div class="form-group">
              <label>Keterangan</label>
              <textarea class="form-control" name="keterangan" placeholder="Keterangan" maxlength="100"></textarea>
            </div>
            <div class="form-group">
              <label>Tahun Ajar</label>
              <input type="text" class="form-control number-only" name="tahun_ajar" placeholder="Tahun Ajar" required maxlength="4"/>
            </div>

        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" name="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url('C_siswa/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("#submit").val("Simpan");
      });
       $('.deaktifasi').click(function(e){
          e.preventDefault();
          var link = $(this).attr('href');

          swal({
              title: "Are you sure want to deactivation?",
              text: "By clicking 'OK' you data will be deleted.",
              type: "warning",
              showCancelButton: true
          },
          function(){
              window.location.href = link;
          });
      });
        $("input[name='nis']").on("input",function(){

        var id =$(this).val(); 
        if(!id){
          id = "ngasal";
        }
        $.ajax({
          url : "<?php echo site_url('C_siswa/validasi_id')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){
            if(response.validasi == "sama"){
              $("input[name='nis']").val("");
              swal({
                  title: "Validasi",
                  text: "Id Mapel Sudah Ada",
                  type :"warning"
              });

              $("input[name='nis']").focus();
            }
          }
        })

      })
       $('.aktifasi').click(function(e){
          e.preventDefault();
          var link = $(this).attr('href');

          swal({
              title: "Are you sure want to deactivation?",
              text: "By clicking 'OK' you data will be deleted.",
              type: "warning",
              showCancelButton: true
          },
          function(){
              window.location.href = link;
          });
      });
      $(".edit").click(function(){
        $("#submit").val("Update");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url('C_siswa/update_data')?>");
        var id = $(this).attr("id");
        $.ajax({
          url : "<?php echo site_url('C_siswa/getDataId')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){
            $("input[name='nis']").val(response.nis);
            $("input[name='nama']").val(response.nama);
            $("input[name='email']").val(response.email);
            $("input[name='password']").val(response.password);
            $("input[name='ttl']").val(response.ttl);
            $("textarea[name='keterangan']").val(response.keterangan);
            $("input[name='tahun_ajar']").val(response.tahun_ajar);

            $('select[name="kelas"] option[value='+ response.id_kelas +']').attr('selected','selected');
            $('select[name="jenis_kelamin"] option[value='+ response.jenis_kelamin +']').attr('selected','selected');
            $('select[name="agama"] option[value='+ response.agama +']').attr('selected','selected');
            $('select[name="status"] option[value='+ response.status +']').attr('selected','selected');
          }
        })

      });

   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
