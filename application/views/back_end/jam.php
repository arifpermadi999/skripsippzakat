<!DOCTYPE html>
<html>
  <head>
  
    <title><?php echo $title; ?></title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>

  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          <?php echo $title; ?>
        </h1>
        <ol class="breadcrumb">
        <li class="active"><?php echo $title; ?></li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="box">
        <div class="box-header with-border">
          <h3 class="box-title">Title</h3>

          <div class="box-tools pull-right">
            <button type="button" class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse">
              <i class="fa fa-minus"></i></button>
            <button type="button" class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove">
              <i class="fa fa-times"></i></button>
          </div>
        </div>
        <div class="box-body">
         
        <?php $this->load->view($alert); ?>

        <button type="button" class="btn btn-default" data-toggle="modal" data-target="#tambahDataModal" id="tambah">
            <i class="glyphicon glyphicon-plus" ></i> 
          Tambah
        </button>
          <br><br>

                 <!-- Main content -->
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Jam</th>
                <th>Action</th>
              </tr>
            </thead>
            <tbody>
              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td>
                    <?php echo $data_db->jam; ?>
                  </td>
                  <td>
                    <a href="#" id="<?php echo $data_db->id_jam ?>" class="edit btn btn-primary fa fa-edit" data-toggle="modal" data-target="#tambahDataModal"> Edit</a>
                    <a href="<?php echo site_url('C_jam/delete_data/'.$data_db->id_jam);?>" id="<?php echo $data_db->id_jam ?>" class="delete btn btn-danger fa fa-trash"> Delete</a>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>
        </div>
        <!-- /.box-body -->
        <div class="box-footer">

        </div>
        <!-- /.box-footer-->
      </div>
      <!-- /.box -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
      <!-- Tambah dan Edit Modal -->
  <div class="modal fade" id="tambahDataModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
          <h4 class="modal-title" id="myModalLabel"></h4>
        </div>

        <form method="post" action="" id="myForm">
        <div class="modal-body">
            <div class="form-group">

            <input type="hidden" name="id_kelas">
              <label>Jam</label>
              <div class="row">
                  <div class="col-lg-6 col-md-6 col-sm-6">
                     <div class="bootstrap-timepicker">
                      <div class="form-group">
                        <label>Jam Mulai</label>
                        <div class="input-group">
                          <input type="text" class="form-control timepicker" name="jam_mulai" required>
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>
                        <!-- /.input group -->
                      </div>
                      <!-- /.form group -->
                    </div>
                  </div>
                  <div class="col-lg-6 col-md-6 col-sm-6">
                     <div class="bootstrap-timepicker">
                      <div class="form-group">
                        <label>Jam Akhir</label>
                        <div class="input-group">
                          <input type="text" class="form-control timepicker" name="jam_akhir" required>
                          <div class="input-group-addon">
                            <i class="fa fa-clock-o"></i>
                          </div>
                        </div>
                        <!-- /.input group -->
                      </div>
                      <!-- /.form group -->
                    </div>
                  </div>
              </div>
            </div>

            
        </div>
        <div class="modal-footer">
          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
          <input class="btn btn-primary" type="submit" value="Simpan" id="submit" name="submit" />
          
        </div>
        </form>
      </div>
    
    </div>
  </div>
  
  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#tambah").click(function(){
        $("#myForm").attr("action","<?php echo site_url('C_jam/insert_data')?>");
        $("#myModalLabel").text("Tambah Data");
        $("input").val("");
        $("#submit").val("Simpan");
      });
      $(".edit").click(function(){
        $("#submit").val("Simpan");
        $("#myModalLabel").text("Edit Data");
        $("#myForm").attr("action","<?php echo site_url('C_jam/update_data')?>");
        var id = $(this).attr("id");
        $.ajax({
          url : "<?php echo site_url('C_jam/getDataId')?>" + "/" + id,
          dataType:"json",
          type : "get",
          success:function(response){
            $("input[name='id_jam']").val(response.id_jam);
            var jam = response.jam;
            var jam_mulai = jam.substr(0,5);
            var jam_akhir = jam.substr(8);
            $("input[name='jam_mulai']").val(jam_mulai);
            $("input[name='jam_akhir']").val(jam_akhir);
 
          }
        })

      });

   
      

    })
    
    

  </script>

  <?php $this->load->view($modal_profile); ?>
</body>
</html>
