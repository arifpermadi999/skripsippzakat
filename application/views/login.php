<!DOCTYPE html>
<html>
<head>

 <?php $this->load->view($meta) ?>

  <title>Log in</title>
  <!-- Tell the browser to be responsive to screen width -->
  
    <?php $this->load->view($css) ?>

  <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
  <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
  <!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
  <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
  <![endif]-->

  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
</head>
<body class="hold-transition login-page">

<!-- /.login-box -->
<div class="login-box">

          <?php if($this->session->userdata('error')) { ?>

                    <div class="alert alert-danger alert-dismissible">
                      <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                      <h4><i class="icon fa fa-ban"></i> Error</h4>
                      Username and Password Wrong
                    </div>
          <?php $this->session->unset_userdata('error');} ?>

        <div class="login-box-body">
        
          <a href="<?php echo site_url('C_front_end')?>">
            <center>
              <img src="<?php echo base_url()."asset/Picture/icon_web.png" ?>"   style="width:40%;">
            </center>
          </a>
          <center><h1>Login</h1></center>
          <br>
          <br>
          <form action="<?php echo site_url('LoginController/login'); ?>" method="post">
            <div class="form-group has-feedback">
              <input required type="email" class="form-control" placeholder="Email" name="email">
              <span class="glyphicon glyphicon-user form-control-feedback"></span>
            </div>
            <div class="form-group has-feedback">
              <input required type="password" class="form-control" placeholder="Password" name="password">
              <span class="glyphicon glyphicon-lock form-control-feedback"></span>
            </div>
            <div class="row">
              <div class="col-xs-4">
                
              </div>
              <!-- /.col -->
              <div class="col-xs-4">
                <button type="submit" class="btn btn-primary btn-block btn-flat">Login</button>
              </div>
              <!-- /.col -->
            </div>
          </form>

          <!-- /.social-auth-links -->
          <br>

        </div>
        <!-- /.login-box-body -->    
  
</div>


  <?php $this->load->view($js) ?>
  <script type="text/javascript">
    $(document).ready(function(){
      $("#repassword_reg").focusout(function(){
          if( $("#repassword_reg").val() != $("#password_reg").val() ){
            alert("Password not match");
            $("#repassword_reg").val("");
          }
      })
      $("#form_register").submit(function(e){
        
        
        if( $("#repassword_reg").val() != $("#password_reg").val() ){
            alert("Password not match");
            $("#repassword_reg").val("");
            e.preventDefault();    
          }
      })
    })
  </script>
</body>
</html>
