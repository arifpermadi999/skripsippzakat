<!DOCTYPE html>
<html>
  <head>
  
    <title>Dashboard</title>
    
    <?php $this->load->view($meta) ?>

    <?php $this->load->view($css) ?>
  </head>
  <style type="text/css">
  .small-box-footer{
    margin-top: 50px;
  }
  </style>
  <body class="hold-transition skin-blue sidebar-mini">
  <!-- Site wrapper -->
  <div class="wrapper">
  
    
  
  <?php $this->load->view($menu) ?>
  
      <!-- =============================================== -->
  
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <h1>
          Dashboard
        </h1>
        <ol class="breadcrumb">
        <li class="active">Dashboard</li>
      </ol>
    </section>

    <!-- Main content -->
    <section class="content">
    <div class="row">
      <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">

    <!-- Default box -->
        <?php if($this->session->userdata('status') == "Siswa"){ ?>
          <h1>Welcome in SMAN 5 Tambun Selatan</h1>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Menu</h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                    <!-- Small boxes (Stat box) -->
                    <div class="col-lg-6 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua" >
                        <div class="inner">
                          <h3 style="font-size:29px;">Jadwal</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-book"></i>
                        </div>
                        <a href="<?php echo site_url('C_jadwal/lihat_jadwal_siswa'); ?>" class="small-box-footer">Lihat Jadwal<i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-green">
                        <div class="inner">
                          <h3 style="font-size:29px;">Nilai</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-book"></i>
                        </div>
                        <a href="<?php echo site_url('C_nilai/lihat_nilai_siswa'); ?>" class="small-box-footer">Lihat Nilai <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->          
          <?php } else if($this->session->userdata('status') == "Guru"){ ?>
          <h1>Welcome in SMAN</h1>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Menu</h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                    <!-- Small boxes (Stat box) -->
                    <div class="col-lg-6 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-aqua">
                        <div class="inner">
                          <h3 style="font-size:29px;">Jadwal</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-book"></i>
                        </div>
                        <a href="<?php echo site_url('C_jadwal/lihat_jadwal_guru'); ?>" class="small-box-footer">Lihat Jadwal<i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
                    <!-- ./col -->
                    <div class="col-lg-6 col-xs-6">
                      <!-- small box -->
                      <div class="small-box bg-green">
                        <div class="inner">
                          <h3 style="font-size:29px;">Nilai</h3>
                        </div>
                        <div class="icon">
                          <i class="fa fa-fw fa-book"></i>
                        </div>
                        <a href="<?php echo site_url('C_nilai'); ?>" class="small-box-footer">Manage Nilai <i class="fa fa-arrow-circle-right"></i></a>
                      </div>
                    </div>
              </div>

            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->          
          <?php } else if($this->session->userdata('status') == "Admin"){ ?>
          <div class="box box-primary">
            <div class="box-header with-border">
              <h3 class="box-title">Master</h3>

              <div class="box-tools pull-right">
                
                <button type="button" class="btn btn-box-tool" data-widget="remove"><i class="fa fa-times"></i></button>
              </div>
            </div>
            <div class="box-body">
              <div class="row">

                
                <!-- ./col -->
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-yellow">
                    <div class="inner">
                      <h3 style="font-size:29px;">Muzakki</h3>
                      <p>Master</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-fw fa-book"></i>
                    </div>
                    <a href="<?php echo site_url('C_mapel'); ?>" class="small-box-footer">Input Muzakki <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-red">
                    <div class="inner">
                      <h3 style="font-size:29px;">Mustahiq</h3>
                      <p>Master</p>
                    </div>
                    <div class="icon">
                      <i class="fa fa-fw fa-book"></i>
                    </div>
                    <a href="<?php echo site_url('C_siswa'); ?>" class="small-box-footer">Input Mustahiq <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->

                <!-- Small boxes (Stat box) -->
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-aqua">
                    <div class="inner">
                      <h3 style="font-size:29px;">Kategori<br>Muzakki</h3>
                    </div>
                    <div class="icon">
                      <i class="fa fa-fw fa-tags"></i>
                    </div>
                    <a href="<?php echo site_url('C_jadwal'); ?>" class="small-box-footer">Input Kategori Muzakki <i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>
                <!-- ./col -->
                <div class="col-lg-6 col-xs-6">
                  <!-- small box -->
                  <div class="small-box bg-green">
                    <div class="inner">
                      <h3 style="font-size:29px;">Kategori<br>Mutashiq</h3>
                    </div>
                    <div class="icon">
                      <i class="fa fa-fw fa-tags"></i>
                    </div>
                    <a href="<?php echo site_url('C_nilai'); ?>" class="small-box-footer">Input Kategori Mutashiq<i class="fa fa-arrow-circle-right"></i></a>
                  </div>
                </div>

              </div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer">
              Master Input Data
            </div>
            <!-- /.box-footer-->
          </div>
          <!-- /.box -->
      
      <?php  } ?>
      </div>
      <div class="col-lg-6 col-md-12 col-sm-12 col-xs-12">
            <div class="box box-solid bg-green-gradient">
            <div class="box-header">
              <i class="fa fa-calendar"></i>

              <h3 class="box-title">Calendar</h3>
              <!-- tools box -->
              <div class="pull-right box-tools">
                
                <button type="button" class="btn btn-success btn-sm" data-widget="collapse"><i class="fa fa-minus"></i>
                </button>
                <button type="button" class="btn btn-success btn-sm" data-widget="remove"><i class="fa fa-times"></i>
                </button>
              </div>
              <!-- /. tools -->
            </div>
            <!-- /.box-header -->
            <div class="box-body no-padding">
              <!--The calendar -->
              <div class="calendar" style="width: 100%" ></div>
            </div>
            <!-- /.box-body -->
            <div class="box-footer text-black" style="height:98px;">
              <center><h1>CALENDAR</h1></center>
            </div>

          </div>
          <!-- /.box -->
      </div> <!-- /row -->

   
          <!-- /.box -->
          
      </div><!-- /row -->
    </div><!-- section -->
      

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->

  
  <?php $this->load->view($footer) ?>


  <?php $this->load->view($sidebar_option) ?>

</div>
<!-- ./wrapper -->
  
  <?php $this->load->view($js) ?>
  <script type="text/javascript">
  $(document).ready(function(){
    var now =new Date();
    
    var date = now.getDate();
    var month = now.getMonth();
    var year = now.getFullYear();
    // The Calender
    $('.calendar').datepicker('update', new Date(year, month, date));
  })
  </script>

  <?php $this->load->view($modal_profile) ?>

</body>
</html>
