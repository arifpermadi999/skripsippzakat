    <div class="row">
      <!-- /.col-lg-3 -->
      <div class="col-lg-8">
         <div id="carouselExampleIndicators" class="carousel slide my-4" data-ride="carousel">
            <ol class="carousel-indicators">
               <?php foreach($data as $row => $data_db){ ?>
               <?php if($row <= 2){ ?>
               <li data-target="#carouselExampleIndicators" data-slide-to="<?= $row ?>" <?php if($row == 0){ echo "class='active'";} ?> ></li>
               <?php 
                  }
                  } ?>
            </ol>
            <div class="carousel-inner" role="listbox">
               <?php foreach($data as $row => $data_db){ ?>
               <?php if($row <= 2){ ?>
               <div class="carousel-item <?php if($row == 0){ echo "active";} ?>">
                  <img class="d-block img-fluid" src="<?= base_url().'asset/Picture/Berita/'.$data_db->photo_berita; ?>" alt="<?= $row + 1 ?> slide" style="width: 100%;height:400">
                  <div class="carousel-caption">
                     <h3 style="background-color:black; "><a href="<?= site_url('FrontEndController/detail_berita/'.$data_db->id_berita)?>"><?= $data_db->judul_berita; ?></a></h3>
                     <p><?= substr($data_db->isi_berita,0,150); ?></p>
                  </div>
               </div>
               <?php 
                  }
                  } ?>  
            </div>
            <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
            </a>
         </div>
         <div class="row">
            <?php foreach($data as $row => $data_db){ ?>
            <?php if($row > 0){ ?>
            <div class="col-lg-4 col-md-6 mb-4">
               <div class="card h-100">
                  <a href="<?= site_url('FrontEndController/detail_berita/'.$data_db->id_berita)?>"><img class="card-img-top" src="<?= base_url().'asset/Picture/Berita/'.$data_db->photo_berita; ?>" alt=""></a>
                  <div class="card-body">
                     <h4 class="card-title">
                        <a href="<?= site_url('FrontEndController/detail_berita/'.$data_db->id_berita)?>"><?= $data_db->judul_berita; ?></a>
                     </h4>
                     <p class="card-text"><?= substr($data_db->isi_berita,0,50); ?></p>
                  </div>
                  <div class="card-footer">
                     <a href="<?= site_url('FrontEndController/detail_berita/'.$data_db->id_berita)?>">
                        <p class="card-text">Read More</p>
                     </a>
                  </div>
               </div>
            </div>
            <?php 
               }
               } ?>
         </div>
         <!-- /.row -->
      </div>
      <!-- /.col-lg-8 -->
      <div class="col-lg-4">
         
          <?php $this->load->view('front_end/component/headline_berita') ?>
      </div>
    </div>