
<style type="text/css">
  h4.with_line{
    text-align: center;
    border-bottom: 2px solid black;
    margin: 10px 0px;
    padding-bottom: 20px;
  }
  h4{
    padding-bottom: 10px;
    text-align: center;
  }
  h4 a{
    color: black !important;
  }
  .card-body{
    padding: 0px;
  }

</style>
<!-- Portfolio Item Heading -->
      <h1 class="my-4">Profil
      </h1>

      <!-- Portfolio Item Row -->
      <div class="row">
        <div class="col-lg-9">
           <form method="post" action="<?= site_url('FrontendController/action_edit_profil') ?>" id="myForm">

                <div class="modal-body">
                    <div class="form-group">
                      <label>No Pendaftaran</label>
                      <input type="text" class="form-control" name="id_muzakki" placeholder="Id Muzakki" required maxlength="5" value="<?= $data->id_muzakki; ?>" readonly/>
                    </div>
                    <div class="form-group">
                      <label>NPWP</label>
                      <input type="text" class="form-control" name="NPWP" placeholder="NPWP" required value="<?= $data->NPWP; ?>"/>
                    </div>
                    <div class="form-group">
                      <label>Nama Muzakki</label>
                      <input type="text" class="form-control" name="nama_muzakki" placeholder="Nama Muzakki" required value="<?= $data->nama_muzakki; ?>" />
                    </div>

                    <div class="form-group">
                      <label>Kategori Muzakki</label>
                      <select class="form-control" name="id_katmuzakki">
                        <?php foreach($data_katmuzakki as $data_db){ ?>
                        <option value="<?php echo $data_db->id_katmuzakki; ?>" <?php if($data_db->id_katmuzakki == $data->id_katmuzakki){ echo "selected";} ?> ><?php echo $data_db->kategori_muzakki; ?></option>
                        <?php } ?>
                      </select>
                    </div>

                    <div class="form-group">
                      <label>Alamat</label>
                      <input type="text" class="form-control" name="alamat" placeholder="Alamat" required value="<?= $data->alamat; ?>"/>
                    </div>
                    <div class="form-group">
                      <label>No Telepon</label>
                      <input type="text" class="form-control" name="no_telepon" placeholder="No Telepon" required value="<?= $data->no_telepon; ?>"/>
                    </div>
                    <div class="form-group">
                      <label>Username</label>
                      <input type="text" class="form-control" name="username" placeholder="Username" required value="<?= $data->username; ?>"/>
                    </div>
                    <div class="form-group">
                      <label>Password</label>
                      <input type="password" class="form-control" name="password" placeholder="Password" required value="<?= $data->password; ?>"/>
                    </div>
                </div>
              <input class="btn btn-primary" type="submit" value="Update" />
              </form>
        </div>
        <?php $this->load->view('front_end/component/menu_profil') ?>
      </div>
      <!-- /.row -->
