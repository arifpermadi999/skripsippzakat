<!-- Portfolio Item Heading -->
<h1 class="my-4"><?= $title_detail; ?></h1>
<!-- Portfolio Item Row -->
<div class="row">
   <div class="col-lg-8">
      <div class="card ">
         <div class="card-body">
            <h3 class="card-title">
               <?= $data['judul']; ?>
            </h3>
            <br>
            <img class="card-img-top" src="<?= $data['photo'] ?>" alt="" >
            <p class="card-text"><?= $data['keterangan']; ?></p>
            <p><?= $data['tanggal']; ?></p>
         </div>
      </div>
   </div>
   <div class="col-lg-4">
      <div class="row">
         <div class="col-lg-12 col-md-12 mb-12">
            <div class="card h-100">
               <div class="card-body">
                  <h3>Berita</h3>
                  <br>
                  <?php foreach($data_berita as $row => $data_db){ ?>
                  <?php if($row < 6){ ?>
                  <div class="row">
                     <div class="col-lg-3 col-md-3 col-sm-3 col-xs-3">
                        <a href="<?= site_url('FrontEndController/detail_berita/'.$data_db->id_berita)?>">
                        <img class="card-img-top" src="<?= base_url().'asset/Picture/Berita/'.$data_db->photo_berita; ?>" alt="">
                        </a>
                     </div>
                     <div class="col-lg-9 col-md-9 col-sm-3 col-xs-9">
                        <h6>  
                           <a href="<?= site_url('FrontEndController/detail_berita/'.$data_db->id_berita)?>"><?= $data_db->judul_berita; ?></a>
                        </h6>
                     </div>
                  </div>
                  <hr style="background-color: black;">
                  <!-- 
                     <p class="card-text"><?= substr($data_db->isi_berita,0,50); ?></p> -->
                  <?php }
                     } ?>
               </div>
               <div class="card-footer">
               </div>
            </div>
         </div>
      </div>
   </div>
</div>

<!-- /.row -->