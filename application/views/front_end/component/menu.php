
    <!-- Navigation -->
    <nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
      <div class="container">
        <img src="<?php echo base_url();?>asset/Picture/icon_web.png" style="width: 80px;height: 90px;margin-right: 20px;">
        <a class="navbar-brand" href="#">SISTEM INFORMASI PENERIMAAN DAN PENYALURAN ZAKAT,<br> INFAK DAN SEDEKAH LAZISMU KOTA BANJARBARU</a>
        <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarResponsive">
          <ul class="navbar-nav ml-auto">
            <li class="nav-item <?php if($on_menu == "Beranda"){ echo "active"; }?> ">
              <a class="nav-link" href="<?= site_url('FrontendController')?>">Beranda
                <?php if($on_menu == "Beranda"){ ?>
                  <span class="sr-only">(current)</span>
                <?php } ?>
              </a>
            </li>
            <!-- <li class="nav-item <?php if($on_menu == "Beranda"){ echo "About"; }?>">
              <a class="nav-link" href="<?= site_url('FrontendController/about')?>">About
              <?php if($on_menu == "About"){ ?>
                  <span class="sr-only">(current)</span>
                <?php } ?>
              </a>
            </li> -->
            <li class="nav-item <?php if($on_menu == "Galeri"){ echo "active"; }?>">
              <a class="nav-link" href="<?= site_url('FrontendController/galeri')?>">Galeri
              <?php if($on_menu == "Galeri"){ ?>
                  <span class="sr-only">(current)</span>
                <?php } ?>
              </a>
            </li>
            <?php if($this->session->userdata('status') == "muzakki"){ ?>
              
              <li class="nav-item <?php if($on_menu == "Profil"){ echo "active"; }?>">
                <a class="nav-link" href="<?= site_url('FrontendController/profil')?>">Profil</a>
              </li>
              <li class="nav-item ">
                <a class="nav-link" href="<?= site_url('FrontendController/logout')?>">Logout</a>
              </li>

            <?php }else{ ?>

              <li class="nav-item <?php if($on_menu == "Pendaftaran"){ echo "active"; }?>">
                <a class="nav-link" href="<?= site_url('FrontendController/pendaftaran')?>">Pendaftaran
                <?php if($on_menu == "Pendaftaran"){ ?>
                    <span class="sr-only">(current)</span>
                  <?php } ?>
                </a>
              </li>

              <li class="nav-item <?php if($on_menu == "Login"){ echo "active"; }?>">
                <a class="nav-link" href="<?= site_url('FrontendController/login')?>">
                <?php if($on_menu == "Login"){ ?>
                    <span class="sr-only">(current)</span>
                  <?php } ?>
                Login</a>
              </li>
            <?php  } ?>
            
          </ul>
        </div>
      </div>
    </nav>