
<!-- Bootstrap core JavaScript -->
<script src="<?php echo base_url() ?>asset/FrontEnd/vendor/jquery/jquery.min.js"></script>
<script src="<?php echo base_url() ?>asset/FrontEnd/vendor/bootstrap/js/bootstrap.bundle.min.js"></script>


<!-- Sweet Alert -->
<script src="<?php echo base_url() ?>asset/SweetAlert/sweetalert.min.js"></script>

<!-- DataTables -->
<script src="<?php echo base_url() ?>asset/BackEnd/plugins/datatables/jquery.dataTables.min.js"></script>

  <script>
    

    $(function () {
      $("#example1").DataTable();
      $("#example2").DataTable();
      
      $('#example3').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": false, 
        "ordering": true,
        "info": true,
        "autoWidth": false
      });
    });
  </script>

