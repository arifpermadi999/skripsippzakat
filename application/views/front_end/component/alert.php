
          <?php if($this->session->flashdata('proses')=='simpan') { ?>

        <?php } else if ($this->session->flashdata('error')) {?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Error</h4>
                <?php echo $this->session->flashdata('error'); ?>
              </div>
        <?php } else if ($this->session->flashdata('proses') == 'pendaftaran') {?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Sukses</h4>
                Pendaftaran sukses
              </div>
        <?php } else if ($this->session->flashdata('proses') == 'edit_profil') {?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Sukses</h4>
                Edit Profil Berhasil
              </div>
        <?php } else if ($this->session->flashdata('proses') == 'konfirmasi_transfer') {?>
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Sukses</h4>
                Konfirmasi Transfer Berhasil Menunggu Konfirmasi Admin
              </div>
        <?php } else if ($this->session->flashdata('proses') == "aktif") {?>
              <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-ban"></i> Aktifasi</h4>
                <?php echo "Siswa Berhasil Diaktifkan"; ?>
              </div>
        <?php } else if ($this->session->flashdata('proses')=='update') {?>
            
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> Update</h4>
                Update Data Sukses
              </div>
        <?php } else if ($this->session->flashdata('proses')=='delete') {?>
            
              <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-hidden="true">&times;</button>
                <h4><i class="icon fa fa-check"></i> delete</h4>
                Delete Data Suskes
              </div>
        <?php }?>
        