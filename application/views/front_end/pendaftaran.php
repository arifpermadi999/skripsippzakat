<!-- Portfolio Item Heading -->
      <h1 class="my-4">Pendaftaran
      </h1>

      <!-- Portfolio Item Row -->
      <div class="row">
        <div class="col-lg-12">
         <form method="post" action="<?= site_url('FrontendController/action_pendaftaran') ?>" id="myForm">
            <div class="modal-body">
                <div class="form-group">
                  <label>No Pendaftaran</label>
                  <input type="text" class="form-control" name="id_muzakki" placeholder="Id Muzakki" required maxlength="5" value="<?= $auto_id; ?>" readonly/>
                </div>
                <div class="form-group">
                  <label>NPWP</label>
                  <input type="text" class="form-control" name="NPWP" placeholder="NPWP" required />
                </div>
                <div class="form-group">
                  <label>Nama Muzakki</label>
                  <input type="text" class="form-control" name="nama_muzakki" placeholder="Nama Muzakki" required />
                </div>

                <div class="form-group">
                  <label>Kategori Muzakki</label>
                  <select class="form-control" name="id_katmuzakki">
                    <?php foreach($data_katmuzakki as $data_db){ ?>
                    <option value="<?php echo $data_db->id_katmuzakki; ?>"><?php echo $data_db->kategori_muzakki; ?></option>
                    <?php } ?>
                  </select>
                </div>

                <div class="form-group">
                  <label>Alamat</label>
                  <input type="text" class="form-control" name="alamat" placeholder="Alamat" required />
                </div>
                <div class="form-group">
                  <label>No Telepon</label>
                  <input type="text" class="form-control" name="no_telepon" placeholder="No Telepon" required />
                </div>
                <div class="form-group">
                  <label>Username</label>
                  <input type="text" class="form-control" name="username" placeholder="Username" required />
                </div>
                <div class="form-group">
                  <label>Password</label>
                  <input type="password" class="form-control" name="password" placeholder="Password" required />
                </div>
            </div>
          <input class="btn btn-primary" type="submit" value="Daftar" />
          </form>          
        </div>

      </div>  
      <!-- /.row -->
