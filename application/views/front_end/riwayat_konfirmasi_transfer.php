<!-- Portfolio Item Heading -->
      <h1 class="my-4">Riwayat Transfer
      </h1>

      <!-- Portfolio Item Row -->
      <div class="row">
        <div class="col-lg-9">
          <table id="example1" class="table table-bordered table-striped table-hovered">
            <thead>
              <tr>
                <th>No</th>
                <th>Tgl Transfer</th>
                <th>Rekening Tujuan</th>
                <th>Jumlah Transfer</th>
                <th>Bukti Transfer</th>
                <th>Status</th>
              </tr>
            </thead>
            <tbody>

              <?php $no=1;foreach ($data as $data_db) { ?>
                <tr>
                  <td>
                    <?php echo $no;$no++; ?>
                  </td>
                  <td ><?php echo $data_db->tgl_transfer; ?></td>
                  <td ><?= $data_db->no_rekening." - ".$data_db->nama." ( ".$data_db->nama_bank." ) "; ?></td>
                  <td ><?php echo $data_db->jumlah_transfer; ?></td>
                  <td >
                    <a href ="<?= base_url().'asset/Picture/Bukti_Transfer/'.$data_db->bukti_transfer; ?>" target="_blank" >
                        <img src="<?= base_url().'asset/Picture/Bukti_Transfer/'.$data_db->bukti_transfer; ?>" style="width:150px;height: 100px;" alt="no-image">
                      </a>
                  </td>
                  <td>
                    <?php if($data_db->status_konfirmasi == 0) { ?>
                      <label class="label " style="background-color: orange;color: white;padding: 10px;">Menunggu Konfirmasi</label>
                    <?php }else { ?>
                      <label class="label " style="background-color: green;color: white;padding: 10px;">Berhasil</label>
                    <?php } ?>
                  </td>
                </tr>
               <?php } ?>
            </tbody>
          </table>             
        </div>

        <?php $this->load->view('front_end/component/menu_profil') ?>
      </div>  
