<!-- Portfolio Item Heading -->
      <h1 class="my-4">Galeri
      </h1>

      <!-- Portfolio Item Row -->
      <div class="row">
        <div class="col-lg-8">
          <div class="row">
            <?php foreach($data as $row => $data_db){ ?>
              <?php if($row > 0){ ?>
                <div class="col-lg-4 col-md-6 mb-4">
                  <div class="card h-100">
                    <a href="<?= site_url('FrontEndController/detail_galeri/'.$data_db->id_galeri)?>">
                      <img class="card-img-top" src="<?= base_url().'asset/Picture/Galeri/'.$data_db->photo_galeri; ?>" alt="" style="width: 100%;height: 200px;">
                    </a>
                    <div class="card-body">
                      <h4 class="card-title">
                        <a href="<?= site_url('FrontEndController/detail_galeri/'.$data_db->id_galeri)?>"><?= $data_db->judul_galeri; ?></a>
                      </h4><!-- 
                      <p class="card-text"><?= substr($data_db->keterangan,0,50); ?></p> -->
                    </div>
                    <div class="card-footer">
                      <a href="<?= site_url('FrontEndController/detail_galeri/'.$data_db->id_galeri)?>"><p class="card-text">Read More</p></a>
                    </div>
                  </div>
                </div>              
              <?php 
                }
              } ?>
          </div>
        </div>

        <div class="col-lg-4">
          <?php $this->load->view('front_end/component/headline_berita') ?>
        </div>
      </div>  
      <!-- /.row -->
