<!DOCTYPE html>
<html lang="en">

  <head>

    <?php $this->load->view($meta); ?>

    <?php $this->load->view($logo_title); ?>

    <title><?= $on_menu ?></title>

    <?php $this->load->view($css); ?>
      
  <style type="text/css">
    h4.with_line{
      text-align: center;
      border-bottom: 2px solid black;
      margin: 10px 0px;
      padding-bottom: 20px;
    }
    h4{
      padding-bottom: 10px;
      text-align: center;
    }
    h4 a{
      color: black !important;
    }

  </style>
  </head>

  <body>


    <?php $this->load->view($menu); ?>
    <!-- Page Content -->
    <div class="container" style="padding: 0px;margin: 20px auto;margin-top:50px;max-width: 95% !important;">

      <?php $this->load->view($alert); ?>
      
      <?php $this->load->view($container); ?>
      
    </div>
    <!-- /.container -->
    <?php $this->load->view($footer); ?>

    <?php $this->load->view($js); ?>
    
  </body>

</html>
