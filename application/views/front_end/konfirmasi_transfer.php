<!-- Portfolio Item Heading -->
      <h1 class="my-4">Konfirmasi Transfer
      </h1>

      <!-- Portfolio Item Row -->
      <div class="row">
        <div class="col-lg-9">
         <form method="post" action="<?= site_url('FrontendController/action_konfirmasi_transfer') ?>" id="myForm" enctype="multipart/form-data">
            <div class="modal-body">
                <div class="form-group">
                  <label>No Transfer</label>
                  <input type="text" class="form-control" name="id_transfer" placeholder="No Transfer" required maxlength="10" value="<?= $auto_id; ?>" readonly/>
                </div>
                <div class="form-group">
                  <label>Tanggal Transfer</label>
                  <input type="text" class="form-control " name="tgl_transfer" placeholder="NPWP" required value="<?= date("Y-m-d")?>" readonly/>
                </div>
                <div class="form-group">
                  <label>Nama Muzakki</label>
                  <input type="text" class="form-control" id="nama_muzakki" placeholder="Nama Muzakki" required value="<?= $this->session->userdata('data')->nama_muzakki; ?>" readonly/>
                  <input type="hidden" class="form-control" name="id_muzakki"  required value="<?= $this->session->userdata('data')->id_muzakki; ?>" />
                </div>

                <div class="form-group">
                  <label>Nama BANK</label>
                  <input type="text" class="form-control can_reset" name="nama_bank_transfer" placeholder="Nama BANK" required />
                </div>
                <div class="form-group">
                  <label>No Rekening</label>
                  <input type="text" class="form-control can_reset" name="no_rekening_transfer" placeholder="No Rekening" required />
                </div>
                <div class="form-group">
                  <label>Atas Nama</label>
                  <input type="text" class="form-control can_reset" name="atas_nama_transfer" placeholder="Atas Nama" required />
                </div>
                <div class="form-group">
                  <label>Bukti Transfer</label>
                  <input type="file" class="form-control can_reset" name="bukti_transfer" placeholder="Bukti Transfer" required />
                </div>
                <div class="form-group">
                  <label>No Rekening Tujuan</label>
                  <select class="form-control" name="id_t_rekening">
                    <?php foreach($data_rekening as $data_db){ ?>
                    <option value="<?php echo $data_db->id_t_rekening; ?>"><?= $data_db->no_rekening." - ".$data_db->nama." ( ".$data_db->nama_bank." ) "; ?></option>
                    <?php } ?>
                  </select>
                </div>
                <div class="form-group">
                  <label>Jumlah Transfer</label>
                  <input type="number" class="form-control" name="jumlah_transfer" placeholder="Jumlah Transfer" required />
                </div>
            </div>
          <input class="btn btn-primary" type="submit" value="Konfirmasi" />
          <div class="btn btn-error" id="reset">Reset</div>
          </form>          
        </div>

        <?php $this->load->view('front_end/component/menu_profil') ?>
      </div>  

      
