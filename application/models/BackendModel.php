

<?php
class BackendModel extends CI_Model{
	function getAllData($menu,$order = NULL){
		if($menu == "Kategori Muzakki"){
			
			return $this->db->get('t_katmuzakki')->result_object();

		}else if($menu == "Kategori Mustahiq"){
			
			return $this->db->get('t_katmustahiq')->result_object();			
		
		}else if($menu == "Rekening"){
			
			return $this->db->get('t_rekening')->result_object();			
		
		}else if($menu == "Berita"){
			if($order){
				return $this->db->order_by('id_berita','desc')->get('t_berita')->result_object();			
			}else{
				return $this->db->get('t_berita')->result_object();				
			}
		
		}else if($menu == "Galeri"){
			if($order){
				return $this->db->order_by('id_galeri','desc')->get('t_galeri')->result_object();			
			}else{
				return $this->db->get('t_galeri')->result_object();				
			}	
		
		}else if($menu == "Program ZIS"){
			if($order){
				return $this->db->order_by('id_program','desc')->get('t_program')->result_object();			
			}else{
				return $this->db->get('t_program')->result_object();				
			}	
		
		}else if($menu == "Saldo"){
			if($order){
				return $this->db->order_by('id_saldo','desc')->get('t_saldo')->result_object();			
			}else{
				return $this->db->get('t_saldo')->result_object();				
			}	
		
		}else if($menu == "Muzakki"){
			$this->db->join('t_katmuzakki','t_katmuzakki.id_katmuzakki = t_muzakki.id_katmuzakki');
			return $this->db->get('t_muzakki')->result_object();			
		
		}else if($menu == "RiwayatTransfer"){
			return $this->db->join('t_rekening','t_rekening.id_t_rekening = t_konfirmasi.id_t_rekening')
							->order_by('status_konfirmasi','asc')
							->where('id_muzakki',$this->session->userdata('data')->id_muzakki)
							->get('t_konfirmasi')->result_object();			
				
		}else if($menu == "RiwayatTransfer2"){
			return $this->db->join('t_rekening','t_rekening.id_t_rekening = t_konfirmasi.id_t_rekening')
							->order_by('status_konfirmasi','asc')
							->get('t_konfirmasi')->result_object();			
				
		}else if($menu == "Mustahiq"){
		
			$this->db->join('t_katmustahiq','t_katmustahiq.id_katmustahiq = t_mustahiq.id_katmustahiq');
			return $this->db->get('t_mustahiq')->result_object();		
			
		}else if($menu == "Penerimaan ZIS"){
			return $this->db->join('t_program','t_program.id_program = t_penerimaan_zis.id_program')
							->join('t_muzakki','t_muzakki.id_muzakki = t_penerimaan_zis.id_muzakki')
							->join('t_konfirmasi','t_konfirmasi.id_transfer = t_penerimaan_zis.id_transfer')
							->join('t_saldo','t_saldo.id_saldo = t_penerimaan_zis.id_saldo')
							->order_by('id_penerimaan_zis','desc')->get('t_penerimaan_zis')->result_object();			
							
		}else if($menu == "Penyaluran ZIS"){
			return $this->db->join('t_program','t_program.id_program = t_penyaluran_zis.id_program')
							->join('t_mustahiq','t_mustahiq.id_mustahiq = t_penyaluran_zis.id_mustahiq')
							->join('t_saldo','t_saldo.id_saldo = t_penyaluran_zis.id_saldo')
							->order_by('id_penyaluran_zis','desc')->get('t_penyaluran_zis')->result_object();			
		}

	}
	function getLogin($data){
		return $this->db->where('username',$data['username'])->where('password',$data['password'])->get('t_muzakki')->row_object();
	}
	function getAllDataById($menu,$id){
		if($menu == "Kategori Muzakki"){
			
			return $this->db->get('t_katmuzakki')->row_object();

		}else if($menu == "Kategori Mustahiq"){
			
			return $this->db->get('t_katmustahiq')->row_object();			
		
		}else if($menu == "Berita"){
			

			return $this->db->where('id_berita',$id)->get('t_berita')->row_object();			
		
		}else if($menu == "Galeri"){

			return $this->db->where('id_galeri',$id)->get('t_galeri')->row_object();			
		
		}else if($menu == "Muzakki"){
			$this->db->join('t_katmuzakki','t_katmuzakki.id_katmuzakki = t_muzakki.id_katmuzakki');
			return $this->db->get('t_muzakki')->row_object();			
		
		}else if($menu == "TransferMuzakki"){
			$this->db->where('t_konfirmasi.id_muzakki',$id);
			$this->db->where('status_konfirmasi',1);
			return $this->db->get('t_konfirmasi')->result_array();			
		
		}else if($menu == "Mustahiq"){
		
			$this->db->join('t_katmustahiq','t_katmustahiq.id_katmustahiq = t_mustahiq.id_katmustahiq');
			return $this->db->get('t_mustahiq')->row_object();		
			
		}

	}
	
	function auto_id($table,$id_name,$pref){
		$auto_id ="";
		if($table == "t_konfirmasi" || $table == "t_penerimaan_zis"){
			$data = $this->db->order_by($id_name,'desc')->get($table)->row_array();
			if($data){
				$id = $data[$id_name];
				$kode=(int) substr($id,-3) + 1;
				$auto_id = $pref.date("ymd").sprintf("%03s",$kode);
			}else{
				$auto_id = $pref.date("ymd")."001";
			}
			
		}else{

			$data = $this->db->order_by($id_name,'desc')->get($table)->row_array();
			if($data){
				$kode=(int) substr($data[$id_name],-3) + 1;  

				$auto_id = $pref.sprintf("%03s",$kode);
			}else{
				$auto_id = $pref."001";
			}
		}
		return $auto_id;
	}
	function delete_data($table,$id_name,$id) {
		$this->db->where($id_name,$id)->delete($table);
		
	}
	function update($table,$id_name,$id,$data) {
		$this->db->where($id_name,$id)->update($table,$data);
		
	}
	function insert($table,$data) {
		$this->db->insert($table,$data);
		
	}
}
