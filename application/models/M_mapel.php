

<?php
class M_mapel extends CI_Model{
	function getAllData() {
		return $this->db->query('select m.*,g.nama from tbl_mapel m,tbl_guru g where m.nip = g.nip')->result_object();
	}
	function getAllDataGuru() {
		return $this->db->get('tbl_guru')->result_object();
	}
	
	function getAllDataById($id) {
		return $this->db->where('id_mapel',$id)->get('tbl_mapel')->row_object();
	}
	function delete($id) {
		$this->db->where('id_mapel',$id)->delete('tbl_mapel');
	}
	function update($id,$data) {
		$this->db->where('id_mapel',$id)->update('tbl_mapel',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_mapel',$data);
	}
}
