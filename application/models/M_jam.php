

<?php
class M_jam extends CI_Model{
	function getAllData() {
		return $this->db->get('tbl_jam')->result_object();
	}
	function getAllDataGuru() {
		return $this->db->get('tbl_guru')->result_object();
	}
	
	function getAllDataById($id) {
		return $this->db->where('id_jam',$id)->get('tbl_jam')->row_object();
	}
	function delete($id) {
		$this->db->where('id_jam',$id)->delete('tbl_jam');
	}
	function update($id,$data) {
		$this->db->where('id_jam',$id)->update('tbl_jam',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_jam',$data);
	}
}
