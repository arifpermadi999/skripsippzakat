

<?php
class M_jadwal extends CI_Model{
	function getAllData($id_kelas,$hari,$semester) {
		return $this->db->query("select j.*,m.mapel,jam.jam from tbl_jadwal j,tbl_mapel m,tbl_jam jam where j.id_kelas = '".$id_kelas."' and j.hari = '".$hari."' and j.semester = '".$semester."' and j.id_mapel = m.id_mapel and j.id_jam = jam.id_jam")->result_object();
	}
	function getAllDataByGuru($hari,$mapel) {
		return $this->db->query("select j.*,m.mapel,jam.jam,k.kelas from tbl_jadwal j,tbl_mapel m,tbl_jam jam,tbl_kelas k where j.id_kelas = k.id_kelas and j.hari = '".$hari."' and j.id_mapel = m.id_mapel and j.id_jam = jam.id_jam and j.id_mapel = '".$mapel."'")->row_object();
	}
	function valid_jadwal($id_kelas,$hari,$semester,$id_jam) {
		return $this->db->query("select j.*,m.mapel,jam.jam from tbl_jadwal j,tbl_mapel m,tbl_jam jam where j.id_kelas = '".$id_kelas."' and j.hari = '".$hari."' and j.semester = '".$semester."' and j.id_mapel = m.id_mapel and j.id_jam = jam.id_jam and j.id_jam = '".$id_jam."'")->result_object();
	}

	function valid_jadwal_mapel($id_kelas,$hari,$semester,$id_mapel) {
		return $this->db->query("select j.*,m.mapel,jam.jam from tbl_jadwal j,tbl_mapel m,tbl_jam jam where j.id_kelas = '".$id_kelas."' and j.hari = '".$hari."' and j.semester = '".$semester."' and j.id_mapel = m.id_mapel and j.id_jam = jam.id_jam and j.id_mapel ='".$id_mapel."'")->result_object();
	}
	function getAllDataKelas() {
		return $this->db->get('tbl_kelas')->result_object();
	}
	function getAllDataMapel() {
		return $this->db->get('tbl_mapel')->result_object();
	}
	function getAllDataWaktu() {
		return $this->db->get('tbl_jam')->result_object();
	}
	
	function delete($id) {
		$this->db->where('id_jadwal',$id)->delete('tbl_jadwal');
	}
	function update($id,$data) {
		$this->db->where('id_jadwal',$id)->update('tbl_jadwal',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_jadwal',$data);
	}
}
