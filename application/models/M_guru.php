

<?php
class M_guru extends CI_Model{
	function getAllData() {
		return $this->db->get('tbl_guru')->result_object();
	}
	
	function getAllDataById($id) {
		return $this->db->where('nip',$id)->get('tbl_guru')->row_object();
	}
	function delete($id) {
		$this->db->where('nip',$id)->delete('tbl_guru');
	}
	function update($id,$data) {
		$this->db->where('nip',$id)->update('tbl_guru',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_guru',$data);
	}
}
