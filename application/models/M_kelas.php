

<?php
class M_kelas extends CI_Model{
	function getAllData() {
		return $this->db->query('select m.*,g.nama from tbl_kelas m,tbl_guru g where m.nip = g.nip')->result_object();
	}
	function getAllDataGuru() {
		return $this->db->get('tbl_guru')->result_object();
	}
	
	function getAllDataById($id) {
		return $this->db->where('id_kelas',$id)->get('tbl_kelas')->row_object();
	}
	function delete($id) {
		$this->db->where('id_kelas',$id)->delete('tbl_kelas');
	}
	function update($id,$data) {
		$this->db->where('id_kelas',$id)->update('tbl_kelas',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_kelas',$data);
	}
}
