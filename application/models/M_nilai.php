

<?php
class M_nilai extends CI_Model{
	function getAllData() {
		return $this->db->query('select m.*,g.nama from tbl_mapel m,tbl_guru g where m.nip = g.nip')->result_object();
	}
	function getAllDataKelas() {
		return $this->db->get('tbl_kelas')->result_object();
	}
	function getAllDataSiswa() {
		return $this->db->get('tbl_siswa')->result_object();
	}
	function getAllDataNilai($id_kelas,$nis){
		return $this->db->where('id_kelas',$id_kelas)->where('nis',$nis)->get('tbl_nilai')->result_object();	
	}
	function getAllDataNilaiLama($nis){
		return $this->db->where('nis',$nis)->get('tbl_nilai')->result_object();	
	}
	function getAllDataById($id) {
		return $this->db->where('id_nilai',$id)->get('tbl_nilai')->row_object();
	}
	function delete($id) {
		$this->db->where('id_nilai',$id)->delete('tbl_nilai');
	}
	function update($id,$data) {
		$this->db->where('id_nilai',$id)->update('tbl_nilai',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_nilai',$data);
	}
}
