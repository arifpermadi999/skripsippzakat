

<?php
class M_student extends CI_Model{
	function getAllData() {
		return $this->db->query('select s.*,k.nama_kelas from tbl_siswa s,tbl_kelas k where s.id_kelas = k.id_kelas')->result_object();
	}
	function getAllDataKelas() {
		return $this->db->get('tbl_kelas')->result_object();
	}
	
	function getAllDataById($id) {
		return $this->db->where('nis',$id)->get('tbl_siswa')->row_object();
	}
	function delete($id) {
		$this->db->where('nis',$id)->delete('tbl_siswa');
	}
	function update($id,$data) {
		$this->db->where('nis',$id)->update('tbl_siswa',$data);
	}
	function insert($data) {
		$this->db->insert('tbl_siswa',$data);
	}
}
